<?php

include("bd.php");
if (!empty($_POST['json_riego'])) {
    $riegos = $_POST['json_riego'];
} else {
    $riegos = "mal";
}

$db = json_decode($riegos, true);
$iteracion = intval(count($db['tipo'])) - 1;
$largo_lista = $iteracion;
$respuesta['largo'] = intval(count($db['tipo']));
$diccionario_tipo = $db['diccionario_tipo'];
$tipo_sector = $db['tipo_sector'];
$config_fertil = $db['config_fertil'];
while ($iteracion >= 0) {
    $tipo = intval($db['tipo']);
    $fechainiJSON = ($db['fechainiJSON']);
    $fechafinJSON = ($db['fechafinJSON']);
    $id_sector = $db['sector'];
    $bomba_continua = ($db['bomba_continua'][0]);
    $sql = "SELECT * FROM `programacion_riego` WHERE id_sector = '$id_sector' and estado <> 6 and estado <> 8 and estado <> 10 
    and ((p_fecha_ini <= '$fechainiJSON' and p_fecha_fin >= '$fechainiJSON') 
    or (p_fecha_ini <= '$fechafinJSON' and p_fecha_fin >= '$fechafinJSON') 
    or (p_fecha_ini <= '$fechainiJSON' and p_fecha_fin >= '$fechafinJSON')) ";
    $consulta = mysql_query($sql, $link) or die(mysql_error());
    if (!$datatmp = mysql_fetch_array($consulta)) {
        if($diccionario_tipo[$tipo_sector[$iteracion]] != 'fertilriego'){
            guardar_sector_tiempo($iteracion,$tipo[$iteracion]); 
        }else{
            guardar_fertil_tiempo($iteracion,$tipo5[$iteracion]);
        }
        $iteracion = $iteracion - 1;
    } else {
        $sql = "select caseta,equipo,nombre_sector,id_nodo,id_empresa from control_riego where id_sector = $id_sector  order by orden";
        $consulta = mysql_query($sql, $link) or die(mysql_error());
        if ($datatmp = mysql_fetch_array($consulta)) {
            $nombre_sector = $datatmp['nombre_sector'];
            $id_nodo = $datatmp['id_nodo'];
            $id_empresa = $datatmp['id_empresa'];
            $equipo = $datatmp['equipo'];
            $caseta = $datatmp['caseta'];
        }

        $respuesta['estado'][$iteracion] = "Problema sincronización";
        $respuesta['id_sector'][$iteracion] = $id_sector;
        $respuesta['id_programacion_riego'][$iteracion] = 0;
        $respuesta['fecha_inicio'][$iteracion] = $fechainiJSON;
        $respuesta['fecha_fin'][$iteracion] = $fechainiJSON;
        $respuesta['nombre_sector'][$iteracion] = $nombre_sector;
        $iteracion = $iteracion - 1;
    }
}
detectar_fertilizante(0);
function guardar_sector_tiempo($iteracion,$tipo_riego){
    global $tipo_riego;
    global $id_sector;
    global $fechainiJSON;
    global $fechafinJSON;
    global $link;
    global $bomba_continua;
    global $id_sectores;
    global $respuesta;
     $sql = "select caseta,equipo,nombre_sector,id_nodo,id_empresa from control_riego where id_sector = '$id_sector[$iteracion]' order by orden";
    $consulta = mysql_query($sql, $link) or die(mysql_error());
    if ($datatmp = mysql_fetch_array($consulta)) {
        $nombre_sector = $datatmp['nombre_sector'];
        $id_nodo = $datatmp['id_nodo'];
        $id_empresa = $datatmp['id_empresa'];
        $equipo = $datatmp['equipo'];
        $caseta = $datatmp['caseta'];
    } 

     $sql = "select red from nodo where id_nodo = $id_nodo";
    $consulta = mysql_query($sql, $link) or die(mysql_error());
    if ($datatmp = mysql_fetch_array($consulta)) {
        $red = $datatmp['red'];
    }
     $sql = "select max(n_identificador) from programacion_riego where id_empresa = '$id_empresa' and equipo = $equipo";
    $consulta = mysql_query($sql, $link) or die(mysql_error());
    if ($datatmp = mysql_fetch_array($consulta)) {
        $n_identificadormax = $datatmp['max(n_identificador)'];
        $n_identificador = $n_identificadormax + 1;
    }
    
    if($tipo_riego == 3){
        $volumen = $fechafinJSON[$iteracion];
         $sql = "select caudal_pulso,caudal_inferior,pulso_minuto from control_riego where id_sector = '$sector[$iteracion]' and caudal_pulso != 0 group by id_sector";
        $consulta = mysql_query($sql,$link) or die(mysql_error());
        if ($datatmp = mysql_fetch_array($consulta)){
            $caudal_pulso = $datatmp['caudal_pulso'];//cn
            $caudal_inferior = $datatmp['caudal_inferior'];//ci
            $pn = $volumen/$caudal_pulso;
            $pi = $caudal_inferior/$caudal_pulso;
            $tmax = $pn/$pi;
            $fecha= strtotime($fechainiJSON[$iteracion]);
            $minutos = round($tmax);
            $minutos = $minutos*60;
            $fecha+=$minutos;
            $fechafinJSON[$iteracion] = date("Y-m-d H:i:s", $fecha ); 
        }
    }

     $sql = "select max(id_programacion_riego) from programacion_riego where id_empresa = '$id_empresa'";
    $consulta = mysql_query($sql, $link) or die(mysql_error());
    if ($datatmp = mysql_fetch_array($consulta)) {
        $lipe = $datatmp['max(id_programacion_riego)']; 
            $id_programacion_riego = $lipe +1;        
    }else{
        $id_programacion_riego = 1;
    }

     $sql2 = "UPDATE control_riego SET  estado = 0,iteracion = 0 WHERE id_sector = '$id_sector[$iteracion]'";
    $consulta = mysql_query($sql2, $link);
    if($bomba_continua == true){
        $fechafinJSON[$iteracion] = date("Y-m-d H:i:s", (strtotime($fechafinJSON[$iteracion]) + 300));     
    }   
     $sql = "INSERT INTO programacion_riego (red,id_programacion_riego,caseta,equipo,id_empresa,n_identificador,n_orden_riego_sector,id_nodo,p_fecha_ini,p_fecha_fin,r_fecha_ini,r_fecha_fin,sector,estado,id_sector) 
    VALUES($red,$id_programacion_riego,$caseta,$equipo,$id_empresa,$n_identificador,1,$id_nodo,'$fechainiJSON[$iteracion]','$fechafinJSON[$iteracion]',
    '$fechainiJSON[$iteracion]','$fechainiJSON[$iteracion]','$nombre_sector',0,$id_sector[$iteracion])";
    $consulta = mysql_query($sql, $link) or die(mysql_error());
    $id_sectores[$iteracion] = $id_programacion_riego; 
    $respuesta['estado'][$iteracion] = "Agendado";
    $respuesta['id_sector'][$iteracion] = $id_sector[$iteracion];
    $respuesta['id_programacion_riego'][$iteracion] = $id_programacion_riego;
    $respuesta['fecha_inicio'][$iteracion] = $fechainiJSON[$iteracion];
    $respuesta['nombre_sector'][$iteracion] = $nombre_sector;
    $respuesta['fecha_fin'][$iteracion] = $fechafinJSON[$iteracion];
}
function guardar_fertil_tiempo($iteracion,$tipo_riego){
    global $tipo_riego;
    global $id_sector;
    global $fechainiJSON;
    global $fechafinJSON;
    global $link;
    global $respuesta;
    global $id_fertilizantes;
    global $config_fertil;
    
    if($tipo_riego == 3){
        $volumen = $fechafinJSON[$iteracion];
         $sql = "select caudal_pulso,caudal_inferior,pulso_minuto from control_riego where id_sector = '$id_sector[$iteracion]' and caudal_pulso != 0 group by id_sector";
        $consulta = mysql_query($sql,$link) or die(mysql_error());
        if ($datatmp = mysql_fetch_array($consulta)){
            $caudal_pulso = $datatmp['caudal_pulso'];//cn
            $caudal_inferior = $datatmp['caudal_inferior'];//ci
            $pn = $volumen/$caudal_pulso;
            $pi = $caudal_inferior/$caudal_pulso;
            $tmax = $pn/$pi;
            $fecha= strtotime($fechainiJSON[$iteracion]);
            $minutos = round($tmax);
            $minutos = $minutos*60;
            $fecha+=$minutos;
            $fechafinJSON[$iteracion] = date("Y-m-d H:i:s", $fecha ); 
        }
    }else{
        $pn = 0;
    }

    $sql = "select nombre_sector,id_empresa,equipo,caseta,id_nodo from control_riego where id_sector = '$id_sector[$iteracion]'";
    $consulta = mysql_query($sql, $link) or die(mysql_error());
    if ($datatmp = mysql_fetch_array($consulta)) {
        $nombre_fertilriego = $datatmp['nombre_sector'];
        $id_nodo = $datatmp['id_nodo'];
        $id_empresa = $datatmp['id_empresa'];
        $equipo = $datatmp['equipo'];
        $caseta = $datatmp['caseta'];
    }

    $sql = "select red from nodo where id_nodo = $id_nodo";
    $consulta = mysql_query($sql, $link) or die(mysql_error());
    if ($datatmp = mysql_fetch_array($consulta)) {
        $red = $datatmp['red'];
    }
    $sql = "select max(n_identificador) from programacion_riego where id_empresa = '$id_empresa' and equipo = $equipo";
    $consulta = mysql_query($sql, $link) or die(mysql_error());
    if ($datatmp = mysql_fetch_array($consulta)) {
        $n_identificadormax = $datatmp['max(n_identificador)'];
        $n_identificador = $n_identificadormax + 1; 
    }        

    $sql = "select max(id_programacion_riego) from programacion_riego where id_empresa = '$id_empresa'";
    $consulta = mysql_query($sql, $link) or die(mysql_error());
    if ($datatmp = mysql_fetch_array($consulta)) {
        $lipe = $datatmp['max(id_programacion_riego)']; 
            $id_programacion_riego = $lipe +1;         
    }else{
        $id_programacion_riego = 1;
    }

    $agitadora = 1;
    $bm = $config_fertil[$iteracion]['bm'];
    $bf = $config_fertil[$iteracion]['bf'];
    $delay_ini = $config_fertil[$iteracion]['delay_ini'];
    $delay_fin = $config_fertil[$iteracion]['delay_fin'];
    $p0 = $config_fertil[$iteracion]['p0'];

    $sql = "INSERT INTO `configuracion_fertil_riegos`(`id_empresa`,`id_sector_fertil_riego`, `agitador`, `bm`,`bf`,`delay_ini`, `delay_fin`,`id_fertil_riego`) VALUES ($id_empresa,'$id_programacion_riego ',$agitadora,$bm,$bf,$delay_ini,$delay_fin,$p0)";
    $consulta = mysql_query($sql, $link) or die(mysql_error());

    $sql = "INSERT INTO programacion_riego (pulsos,red,id_programacion_riego,caseta,equipo,id_empresa,n_identificador,n_orden_riego_sector,id_nodo,p_fecha_ini,p_fecha_fin,r_fecha_ini,r_fecha_fin,sector,estado,id_sector) 
    VALUES($pn,$red,$id_programacion_riego,$caseta,$equipo,$id_empresa,$n_identificador,1,$id_nodo,'$fechainiJSON[$iteracion]','$fechafinJSON[$iteracion]',
    '$fechainiJSON[$iteracion]','$fechainiJSON[$iteracion]','$nombre_fertilriego',0,$id_sector[$iteracion])";
    $consulta = mysql_query($sql, $link) or die(mysql_error());

    $id_fertilizantes[$iteracion] = $id_programacion_riego; 
    $respuesta['estado'][$iteracion] = "Agendado";
    $respuesta['id_sector'][$iteracion] = $id_sector[$iteracion];
    $respuesta['id_programacion_riego'][$iteracion] = $id_programacion_riego;
    $respuesta['fecha_inicio'][$iteracion] = $fechainiJSON[$iteracion];
    $respuesta['nombre_sector'][$iteracion] = $nombre_fertilriego;
    $respuesta['fecha_fin'][$iteracion] = $fechafinJSON[$iteracion];
    $respuesta['config_fertil'][$iteracion] = $config_fertil[$iteracion];
}
function detectar_fertilizante($iteracion){
    global $diccionario_tipo;
    global $tipo_sector;
    global $largo_lista;
    global $id_fertilizantes;
    global $id_sectores;
    global $respuesta;
    global $link; 
    while ($iteracion <= $largo_lista) {
        if($diccionario_tipo[$tipo_sector[$iteracion]] != 'fertilriego'){
            $indice = $iteracion - 1;          
            while ($indice >= 0) {
                
                if($diccionario_tipo[$tipo_sector[$indice]] != 'fertilriego'){
                    
                    $indice = -1; 
                }else{                    
                    $sql2 = "UPDATE configuracion_fertil_riegos SET id_programacion_riego = $id_sectores[$iteracion] WHERE id_sector_fertil_riego = $id_fertilizantes[$indice]";
                    $consulta = mysql_query($sql2, $link);
                }
                $indice--;
            }
        }
        $iteracion++; 
    }
    $respuesta['diccionario_tipo'] = $diccionario_tipo;
    $respuesta['tipo_sector'] = $tipo_sector;
    $respuesta['largo_lista'] = $largo_lista;
    $respuesta['id_fertilizantes'] = $id_fertilizantes;
    $respuesta['id_sectores'] = $id_sectores;
} 
echo json_encode($respuesta);
 