﻿-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 17-08-2018 a las 15:48:05
-- Versión del servidor: 5.6.19-0ubuntu0.14.04.1-log
-- Versión de PHP: 5.5.9-1ubuntu4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `temporal_lem_db`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `programacion_riego`
--

CREATE TABLE IF NOT EXISTS `programacion_riego` (
  `id_programacion_riego` int(10) NOT NULL AUTO_INCREMENT,
  `tipo` varchar(2) NOT NULL DEFAULT 'A',
  `id_empresa` int(10) NOT NULL,
  `n_orden_riego_sector` int(10) DEFAULT NULL,
  `iteracion` int(3) NOT NULL,
  `id_subriego` int(5) NOT NULL,
  `id_nodo` int(10) NOT NULL,
  `fecha_consulta` timestamp NULL DEFAULT NULL,
  `p_fecha_ini` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `p_fecha_fin` timestamp NULL DEFAULT NULL,
  `sector` varchar(30) NOT NULL,
  `r_fecha_ini` datetime NOT NULL,
  `r_fecha_fin` datetime NOT NULL,
  `estado` int(10) NOT NULL,
  `id_sector` int(10) NOT NULL,
  `id_campo` int(5) NOT NULL,
  `intentos` int(10) NOT NULL,
  `error` varchar(100) NOT NULL DEFAULT '0',
  `update_p` int(1) NOT NULL DEFAULT '0',
  `n_identificador` int(15) NOT NULL,
  `equipo` int(11) NOT NULL,
  `caseta` int(11) NOT NULL,
  `pulsos` int(11) NOT NULL,
  `ultimo_pulso` int(11) NOT NULL,
  `pulso_acumulado` int(11) NOT NULL,
  `prioridad` int(11) NOT NULL,
  `primer_pulso` int(11) NOT NULL,
  `red` int(11) NOT NULL,
  `reset` int(11) NOT NULL,
  PRIMARY KEY (`id_programacion_riego`,`id_empresa`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
