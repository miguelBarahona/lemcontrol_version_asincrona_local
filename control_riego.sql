-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 09-10-2018 a las 15:28:09
-- Versión del servidor: 5.5.50-0ubuntu0.14.04.1
-- Versión de PHP: 5.5.9-1ubuntu4.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `temporal_lem_db`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `control_riego`
--

CREATE TABLE IF NOT EXISTS `control_riego` (
  `id_control_riego` int(10) NOT NULL AUTO_INCREMENT,
  `id_sector` int(10) NOT NULL,
  `id_empresa` int(10) NOT NULL,
  `id_sensor` int(11) NOT NULL,
  `nombre_sector` varchar(50) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
  `poligono` varchar(10000) CHARACTER SET utf8 NOT NULL,
  `id_nodo` int(10) NOT NULL,
  `estado` int(2) NOT NULL,
  `p0` int(3) NOT NULL,
  `p1` int(3) NOT NULL,
  `p2` int(3) NOT NULL,
  `orden` int(10) NOT NULL,
  `intentos` int(3) NOT NULL,
  `intentos_reset` int(11) NOT NULL,
  `iteracion` int(11) NOT NULL,
  `fecha` datetime NOT NULL,
  `tipo` int(11) NOT NULL,
  `tipo_elemento` varchar(10) NOT NULL,
  `equipo` int(11) NOT NULL,
  `nombre_equipo` varchar(60) NOT NULL DEFAULT 'Equipo 1',
  `caseta` int(11) NOT NULL,
  `nombre_caseta` varchar(60) NOT NULL DEFAULT 'Caseta 1',
  `caudal_inferior` int(11) NOT NULL,
  `caudal_superior` int(11) NOT NULL,
  `update_p` int(1) NOT NULL DEFAULT '1',
  `presion_inferior` int(11) NOT NULL,
  `presion_superior` int(11) NOT NULL,
  `caudal_normal` int(11) NOT NULL DEFAULT '0',
  `pulso_minuto` int(11) NOT NULL,
  `caudal_pulso` int(11) NOT NULL,
  `reset` int(1) NOT NULL,
  `completo` int(1) NOT NULL,
  `delay` int(11) NOT NULL,
  `tiempo_diff_inicio` int(11) NOT NULL,
  `tiempo_diff_final` int(11) NOT NULL,
  PRIMARY KEY (`id_control_riego`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=303 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
