CREATE TABLE IF NOT EXISTS `transmisor_programacion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `n_orden_riego_sector` int(11) NOT NULL,
  `intentos` int(11) NOT NULL,
  `estado` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=0;

CREATE TABLE IF NOT EXISTS `nombres_planificaciones` (
  `id_planificacion` int(11) NOT NULL AUTO_INCREMENT,
  `id_empresa` int(11) NOT NULL,
  `id_caseta` int(11) NOT NULL,
  `id_equipo` int(11) NOT NULL,
  `nombre_planificacion` varchar(30) NOT NULL,
  PRIMARY KEY (`id_planificacion`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=0 ;

CREATE TABLE IF NOT EXISTS `planificaciones_riegos` (
  `id_planificaciones_riegos` int(11) NOT NULL AUTO_INCREMENT,
  `id_planificacion` int(11) NOT NULL,
  `id_sector` int(10) NOT NULL,
  `fecha_ini` datetime NOT NULL,
  `fecha_fin` datetime NOT NULL,
  `tipo` int(11) NOT NULL,
  PRIMARY KEY (`id_planificaciones_riegos`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=0 ;