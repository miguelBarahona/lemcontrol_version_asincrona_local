-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 11-10-2018 a las 11:41:25
-- Versión del servidor: 5.5.50-0ubuntu0.14.04.1
-- Versión de PHP: 5.5.9-1ubuntu4.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `temporal_lem_db`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sector_configuracion`
--

CREATE TABLE IF NOT EXISTS `sector_configuracion` (
  `id_sector` int(11) NOT NULL,
  `tipo_riego` varchar(20) NOT NULL,
  `equipo` int(11) NOT NULL,
  `caseta` int(11) NOT NULL,
  `caudal_inferior` int(11) NOT NULL,
  `caudal_superior` int(11) NOT NULL,
  `presion_inferior` int(11) NOT NULL,
  `presion_superior` int(11) NOT NULL,
  `completo` int(11) NOT NULL,
  `delay` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
