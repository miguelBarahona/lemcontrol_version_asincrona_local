<?php

//***********************************************************************************************************************
// Lem control v2.0
//
// Ubicacion: servidor MAESTRO (../control/ajax)
//
// Especificacion del archivo:
// este archivo va a preguntar a sincronizar_riego2.php para traspasar riegos al servidor MAESTRO
// el disparador de este archivo es cuando el usuario presiona el boton de accion (agendar,eliminar,resetear), se ejecuta
// cuando tiene conexion con el pc local, y aun no se dispara el sincronizar cada 2 minutos
//
//***********************************************************************************************************************


include("conexion.php");
$swcurl = 0;
$id_empresa = $_POST['id_empresa'];

//query para verificar la ip del pc asociado a la empresa
$sql = "select ip_local from sector_campo where id_empresa = '$id_empresa' ";
	$consulta = mysql_query($sql, $link) or die(mysql_error());
	if ($datatmp = mysql_fetch_array($consulta)) {
		$ip_local = $datatmp['ip_local'];
		//$ip_local='200.24.229.186';
		$direccion='http://'.$ip_local.'/p/sincronizar_riego2.php';
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $direccion);
		curl_setopt($ch, CURLOPT_POST, 1);
		//curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		//curl_setopt($ch, CURLOPT_POSTFIELDS, "verifica=" . $data_post . "");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
		curl_setopt($ch, CURLOPT_TIMEOUT, 5);

		$db = curl_exec($ch);
		
		
		if (!$db[0] != '{' ){
			$db = json_decode($db, true);
			//iteracion trae los riegos por sector al servidor maestro
			if (isset($db['programacion_riego'][0]["largo"])) {
				$x=0;
				while($x < $db['programacion_riego'][0]["largo"]){
					$swcurl = 1;
				$id_programacion_riego = $db['programacion_riego'][$x]['id_programacion_riego']; 	
				$tipo = $db['programacion_riego'][$x]['tipo']; 	
				$id_empresa = $db['programacion_riego'][$x]['id_empresa'];
				$n_orden_riego_sector = $db['programacion_riego'][$x]['n_orden_riego_sector'];
				$iteracion = $db['programacion_riego'][$x]['iteracion'];
				$id_subriego = $db['programacion_riego'][$x]['id_subriego'];
				$id_nodo = $db['programacion_riego'][$x]['id_nodo'];
				$fecha_consulta = $db['programacion_riego'][$x]['fecha_consulta'];
				$p_fecha_ini = $db['programacion_riego'][$x]['p_fecha_ini'];
				$p_fecha_fin = $db['programacion_riego'][$x]['p_fecha_fin'];
				$sector = $db['programacion_riego'][$x]['sector'];
				$r_fecha_ini = $db['programacion_riego'][$x]['r_fecha_ini'];
				$r_fecha_fin = $db['programacion_riego'][$x]['r_fecha_fin'];
				$estado = $db['programacion_riego'][$x]['estado'];
				$id_sector = $db['programacion_riego'][$x]['id_sector'];
				$id_campo = $db['programacion_riego'][$x]['id_campo'];
				$intentos = $db['programacion_riego'][$x]['intentos'];
				$error = $db['programacion_riego'][$x]['error'];
				$update_p = 1;
				$n_identificador = $db['programacion_riego'][$x]['n_identificador'];
				$equipo = $db['programacion_riego'][$x]['equipo'];
				$caseta = $db['programacion_riego'][$x]['caseta'];
				$pulsos = $db['programacion_riego'][$x]['pulsos'];
				$ultimo_pulso = $db['programacion_riego'][$x]['ultimo_pulso'];
				$pulso_acumulado = $db['programacion_riego'][$x]['pulso_acumulado'];
				$prioridad = $db['programacion_riego'][$x]['prioridad'];
				$primer_pulso = $db['programacion_riego'][$x]['primer_pulso'];
				$red = $db['programacion_riego'][$x]['red'];


				
				
				$sql = "select * from programacion_riego where id_programacion_riego = $id_programacion_riego and id_empresa=$id_empresa";
				$consulta = mysql_query($sql, $link);
				$objeto = mysql_num_rows($consulta);
				if($datatmp = mysql_fetch_array($consulta)) {
					if($estado==1){
						$sql="UPDATE programacion_riego set p_fecha_fin='$p_fecha_fin',r_fecha_fin='$r_fecha_fin' 
						where id_programacion_riego = $id_programacion_riego and id_empresa=$id_empresa";
						$consulta=mysql_query($sql, $link);
					}else{
						
						$sql="UPDATE programacion_riego set id_programacion_riego = $id_programacion_riego,
						tipo = '$tipo' ,
						id_empresa = $id_empresa,
						n_orden_riego_sector =$n_orden_riego_sector ,
						iteracion =$iteracion,
						id_subriego =$id_subriego,
						id_nodo =$id_nodo,
						fecha_consulta ='$fecha_consulta',
						p_fecha_ini ='$p_fecha_ini' ,
						p_fecha_fin ='$p_fecha_fin' ,
						sector ='$sector' ,
						r_fecha_ini ='$r_fecha_ini' ,
						r_fecha_fin ='$r_fecha_fin' ,
						estado =$estado,
						id_sector =$id_sector ,
						id_campo =$id_campo ,
						intentos =$intentos ,
						error ='$error' ,
						update_p =1 ,
						n_identificador =$n_identificador ,
						equipo =$equipo,
						caseta =$caseta ,
						pulsos =$pulsos ,
						ultimo_pulso =$ultimo_pulso ,
						pulso_acumulado = $pulso_acumulado ,
						prioridad =$prioridad ,
						primer_pulso =$primer_pulso,
						red =$red 
						where id_programacion_riego = $id_programacion_riego and id_empresa=$id_empresa ";
						$consulta=mysql_query($sql, $link); 
					}
				}else{ 
					$sql = "INSERT INTO programacion_riego 
					(id_programacion_riego ,
					tipo ,
					id_empresa ,
					n_orden_riego_sector ,
					iteracion ,
					id_subriego ,
					id_nodo ,
					fecha_consulta ,
					p_fecha_ini ,
					p_fecha_fin ,
					sector ,
					r_fecha_ini ,
					r_fecha_fin ,
					estado ,
					id_sector ,
					id_campo ,
					intentos ,
					error ,
					update_p ,
					n_identificador ,
					equipo ,
					caseta ,
					pulsos ,
					ultimo_pulso ,
					pulso_acumulado ,
					prioridad ,
					primer_pulso ,
					red) 
					VALUES 
					($id_programacion_riego,
					'$tipo' ,
					$id_empresa,
					$n_orden_riego_sector ,
					$iteracion,
					$id_subriego,
					$id_nodo,
					'$fecha_consulta',
					'$p_fecha_ini' ,
					'$p_fecha_fin' ,
					'$sector' ,
					'$r_fecha_ini' ,
					'$r_fecha_fin' ,
					$estado,
					$id_sector ,
					$id_campo ,
					$intentos ,
					'$error' ,
					1 ,
					$n_identificador ,
					$equipo,
					$caseta ,
					$pulsos ,
					$ultimo_pulso ,
					$pulso_acumulado ,
					$prioridad ,
					$primer_pulso,
					$red)" ;
					$consulta=mysql_query($sql, $link); 
				}
				
				$respuesta_para_local['programacion_riego'][$x]['id_programacion_riego']=$id_programacion_riego;
				$respuesta_para_local['programacion_riego'][$x]['estado']=$estado;
				
				$x++;
					
				}
			}
			//------------------------------------------------------------------------------------------------------------------------------------------------

			
			//iteracion trae los riegos realizados por las valvulas al servidor maestro
			if (isset($db['programacion_val'][0]["largo"])) {
				$x=0;
				while($x < $db['programacion_val'][0]["largo"]){
					$swcurl = 1;
					$id_programacion_riego = $db['programacion_val'][$x]['id_programacion_riego']; 	
					$id_empresa = $db['programacion_val'][$x]['id_empresa'];
					$id_nodo = $db['programacion_val'][$x]['id_nodo'];
					$px = $db['programacion_val'][$x]['px'];
					$estado = $db['programacion_val'][$x]['estado'];
					$nombre = $db['programacion_val'][$x]['nombre'];
					$p_fecha_ini = $db['programacion_val'][$x]['p_fecha_ini'];
					$p_fecha_fin = $db['programacion_val'][$x]['p_fecha_fin'];
					$r_fecha_ini = $db['programacion_val'][$x]['r_fecha_ini'];
					$r_fecha_fin = $db['programacion_val'][$x]['r_fecha_fin'];
					$caseta = $db['programacion_val'][$x]['caseta'];
					$equipo = $db['programacion_val'][$x]['equipo'];


						
					$sql = "select * from programacion_val where id_programacion_riego = $id_programacion_riego";
					$consulta = mysql_query($sql, $link);
					$objeto = mysql_num_rows($consulta);
					if($datatmp = mysql_fetch_array($consulta)) {
						$sql="UPDATE programacion_val set id_programacion_riego = $id_programacion_riego,id_empresa = $id_empresa,id_nodo = $id_nodo,update_p = 1,px = $px,estado  = $estado,nombre = '$nombre',p_fecha_ini = '$p_fecha_ini',p_fecha_fin = '$p_fecha_fin',r_fecha_ini = '$r_fecha_ini',r_fecha_fin = '$r_fecha_fin',caseta = $caseta,equipo = $equipo where id_programacion_riego = $id_programacion_riego";
						$consulta=mysql_query($sql, $link); 
					}else{ 
						$sql = "INSERT INTO programacion_val (id_programacion_riego,id_empresa,id_nodo,update_p,px,estado,nombre,p_fecha_ini,p_fecha_fin,r_fecha_ini,r_fecha_fin,caseta,equipo) 
						VALUES ($id_programacion_riego,$id_empresa,$id_nodo,1,$px,$estado,'$nombre','$p_fecha_ini','$p_fecha_fin','$r_fecha_ini','$r_fecha_fin',$caseta,$equipo)"; 
						$consulta=mysql_query($sql, $link); 
					}
					
					$respuesta_para_local['programacion_val'][$x]['id_programacion_riego']=$id_programacion_riego;
					$respuesta_para_local['programacion_val'][$x]['estado']=$estado;
					
					$x++;
					
				}
			}
			//------------------------------------------------------------------------------------------------------------------------------------------------

			
			//iteracion que se encarga de updatear la tabla control_riego, donde estan los estados de los componentes de los sectores
			if (isset($db['control_riego'][0]["largo"])) {
				$x=0;
				while($x < $db['control_riego'][0]["largo"]){
					$swcurl = 1;
					$estado = $db['control_riego'][$x]['estado']; 
					$fecha = $db['control_riego'][$x]['fecha'];
					$id_sector= $db['control_riego'][$x]['id_sector'];	
					$id_nodo= $db['control_riego'][$x]['id_nodo'];		
						
					$sql="UPDATE control_riego set estado = $estado,fecha = '$fecha'
					where id_nodo=$id_nodo and id_sector= $id_sector";
					$consulta=mysql_query($sql, $link);
					
					$respuesta_para_local['control_riego'][$x]['id_nodo']=$id_nodo;
					$respuesta_para_local['control_riego'][$x]['id_sector']=$id_sector;
					$respuesta_para_local['control_riego'][$x]['estado']=$estado;
					
					$x++;
				}	
			}
			//-----------------------------------------------------------------------------------------------------------------------

			
			//iteracion que se encarga de updatear la tabla control_val, donde estan los estados de los componentes de las valvulas
			if (isset($db['control_val'][0]["largo"])) {
				$x=0;
				while($x < $db['control_val'][0]["largo"]){
					$swcurl = 1;
					$estado = $db['control_val'][$x]['estado']; 
					$id_sector= $db['control_val'][$x]['id_sector'];	
					$id_nodo= $db['control_val'][$x]['id_nodo'];
					$px= $db['control_val'][$x]['px'];	
					$sql="UPDATE control_val set estado = $estado,update_p = 1
					where id_nodo=$id_nodo and id_sector= $id_sector and px = $px";
					$consulta=mysql_query($sql, $link);
					
					$respuesta_para_local['control_val'][$x]['id_nodo']=$id_nodo;
					$respuesta_para_local['control_val'][$x]['id_sector']=$id_sector;
					$respuesta_para_local['control_val'][$x]['px']=$px;
					$respuesta_para_local['control_val'][$x]['estado']=$estado;
					
					$x++;
				}	
			}
			//-----------------------------------------------------------------------------------------------------------------------

			if($swcurl == 1){
				$direccion='http://'.$ip_local.'/p/confirma_tecnoera_asincrono.php';
				$respuesta = json_encode($respuesta_para_local);
				
				//$respuesta='ok';
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $direccion);
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
				curl_setopt($ch, CURLOPT_POSTFIELDS, "respuesta=" . $respuesta . "");
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
				curl_setopt($ch, CURLOPT_TIMEOUT, 5);

				echo $respuesta = curl_exec($ch);
				
			}
			
			
			
		}else{
			echo $respuesta = 404;
		}
		
		
		
	}else{
		echo $respuesta = 404;
	}

?>