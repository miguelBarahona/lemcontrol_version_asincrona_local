<?php
include("ajax/conexion.php");
include("php/main.php");
?>

<html>

<head>
    <title>Timeline LemControl</title>

    <!-- for mobile devices like android and iphone -->
    <meta charset="utf-8">
    <meta content="True" name="HandheldFriendly" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />

    <script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
    <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="graficolineal/graph.js"></script>
    <link rel="stylesheet" type="text/css" href="graficolineal/graph.css">
    <script type="text/javascript" src="timeline/google.jsp"></script>
    <script type="text/javascript" src="timeline/timeline.js"></script>
    <script type="text/javascript" src="timeline/timeline-locales.js"></script>
    <script src="timeline/lib/moment.js"></script>
    <script src="timeline/lib/es.js"></script>
    <script src="timeline/kendo/jquery.min.js"></script>
    <script src="timeline/lib/bootstrap-datetimepicker.js"></script>
    <script src="timeline/bootstrap.min.js"></script>
    <script src="timeline/lib/sweet-alert.js"></script>
    <link rel="stylesheet" type="text/css" href="timeline/lib/sweet-alert.css">
    <link href="timeline/bootstrap.css" rel="stylesheet">
    <link href="timeline/bootstrap-theme.min.css" rel="stylesheet">
    <link href="timeline/lib/bootstrap-datetimepicker.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="timeline/timeline.css">
    <link rel="stylesheet" href="timeline/nav.css">
    <link rel="stylesheet" href="timeline/kendo/kendo.common-material.min.css" />
    <link rel="stylesheet" href="timeline/kendo/kendo.material.min.css" />
    <script type="text/javascript" src="timeline/uds_api_contents.js"></script>
    <script src="timeline/kendo/kendo.all.min.js"></script>
    <script src="timeline/kendo.culture.es-ES.js" type="text/javascript"></script>
    <link rel="stylesheet" href="css/style_timeline.css" />
    <script src="amcharts/amcharts.js"></script>
    <script src="amcharts/serial.js"></script>
    <script src="amcharts/themes/light.js"></script>
</head>

<body id="bodypage" onresize="timeline.redraw();" onload="moverDOM();">

    <div id="abuelo" style="text-align:center;overflow:hidden;">
        <!-- div append popup info riegos -->
        <div id="info2"></div>
        <!-- //////////////////////////// -->
        <!-- NAVBAR TIMELINE              -->
        <nav class="navbar navbar-default" style="margin-bottom: 0px;">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button onClick="toggle_celular();" type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">
                        <img src="timeline/img/blanco2.png" alt="" style="width: 220px;">
                    </a>
                    <label class="hidden-sm " style="margin: -4px;position: relative;top: 8px;font-size: 29px;color: rgb(0, 202, 219);">|</label>
                    <a class="hidden-sm hidden-xs" href="#"><label style="font-family: Segoe UI,Segoe,Tahoma,Arial,Verdana,sans-serif;margin-left: 9px;color: rgb(148, 148, 148);position: relative;font-size: 1.2em;margin-right:  9px;top: 8px;">Planificación Riego</label></a>

                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" style="overflow:  hidden;">
                    <ul class="nav navbar-nav">

                        <li class="dropdown">
                            <select onChange="reloadcaseta(value);" id="casetas" class="form-control" style="top: 11px;position:  relative;">
                                <?php
                                $largoequipo = count($equipo);
                                $b = 0;
                                $c = 0;
                                while ($b < $largoequipo) {
                                    if ($nombre_equipo_post == $nombre_equipo[$b]) {
                                        echo '<option  value="' . $equipo[$b] . '">' . $nombre_equipo[$b] . ' </option>';
                                    }
                                    $b++;
                                }
                                while ($c < $largoequipo) {
                                    if ($nombre_equipo_post != $nombre_equipo[$c]) {
                                        echo '<option  value="' . $equipo[$c] . '">' . $nombre_equipo[$c] . ' </option>';
                                    }
                                    $c++;
                                }
                                ?>
                            </select>
                        </li>
                        <div class='input-group date hidden-xs hidden-sm' style="position: relative;top: 11px;left: 6px;/* left: 299px; */width: 122px;"><input id='datetimepicker6' onChange="setTime();" type='text' class="form-control" /></div>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li class="hidden-sm hidden-xs margin" id="pausa">
                            <img onclick="detectar_pausa();" title="Alertas" id="alerta" src="imagenes/pausa.png" alt="Eliminar" style="cursor:pointer; width: 34px;top: 13px;position:relative;    margin-left: 5px;">
                        </li>
                        <li class="hidden-sm hidden-xs margin" id="play">
                            <img onclick="detectar_play();" title="Alertas" id="alerta" src="imagenes/play.png" alt="Eliminar" style="cursor:pointer; width: 34px;top: 13px;position:relative;    margin-left: 5px;">
                        </li>
                        <li class="hidden-sm hidden-xs margin">
                            <img onclick="notificiaciones();" title="Alertas" id="alerta" src="imagenes/alerta_yellow.png" alt="Eliminar" style="width: 34px;top: 13px;position:relative;    margin-left: 5px;">
                        </li>
                        <li class="hidden-sm hidden-xs margin">
                            <img style="width:34px;position:relative;top:13px;cursor:pointer;    margin-left: 5px;" onclick="doDelete(3)" title="Eliminar Riego" src="timeline/img/cruz.png" alt="Eliminar">
                        </li>
                        <li class="hidden-sm hidden-xs margin">
                            <img data-toggle='modal' data-target='#descargarExcel' title="Descargar Excel" src="imagenes/excel.png" style="width:34px;top:13px;position:relative;cursor:pointer;    margin-left: 5px;">
                        </li>
                        <li class="hidden-sm hidden-xs margin">
                            <img onClick="activacionManualNodos();" data-toggle='modal' data-target='#editarNodos' title="Riegos Manuales" id="cuaderno" src="timeline/img/lista.png" style="width:34px;cursor:pointer;top: 13px;position: relative;    margin-left: 5px;">
                        </li>
                        <li class="hidden-xs hidden-sm margin">
                            <img id="actualizar" onClick="mostrarpersostatos();" title="Estado Presostato" src="timeline/img/valvula.png" style="cursor: pointer;top: 13px;position: relative;width: 34px;    margin-left: 5px;">
                        </li>
                        <li class="visible-xs ">
                            <img style="width:31px;position:relative;margin-top:  5%;" onclick="doDelete();" title="Eliminar Riego" src="timeline/img/cruz.png" alt="Eliminar">
                        </li>
                        <!-- /////////////  --->
                    </ul>
                </div>
            </div>
        </nav>
        <!-- ////////////////////////  -->
        <form action="timelinelem.php" method="Post" name="myForm" id="myForm" data-ajax="false" style="
              display: initial;
              ">
            <input name="equipo" id="equipo" type="hidden" value="" visible="false">
            <input name="id_empresa" id="id_empresa" type="hidden" value="" visible="false">
        </form>
        <div id="padre" style="height: 96%;">
            <div id="infoline"></div>
            <div id="divContenedor">
                <div id="mytimeline" onmouse="borrarinfo();" style="height: 100%; width: 100%; position: absolute; display: block; z-index: 0; left: 0px; top: 0px;"></div>
                <div id="mytimeline2" style="height: 100%; width: 100%; visibility: hidden; overflow-x: hidden; position: absolute;"></div>
            </div>
            <div class="hidden-xs" id="legend_lineal" style="left:1%;top: 2.4%;left: 0.5%;position: relative;"></div>
            <div style="border-style: groove;border-top-width: 1px;"></div>
            <div class="hidden-xs" id="contenendor_lineal">
                <div id="chartdiv" onmouseout="borrarinfo();" style="position: relative; overflow: hidden; text-align: left;"></div>
            </div>

            <div id="arrow_up" class="arrow-up" style="display:none; position: relative;top: -161%;left: 66%;"></div>
            <div id="listaAlert" style="position: relative;width: 30%;overflow-y:scroll;max-height:80%;top: -120%;left: 39%; z-index: 5;background-color: white; border-radius: 10px; box-shadow: 15px 15px 20px #6f6c6c;">
                <ol id="lista2" style="display:none; "></ol>
            </div>
        </div>
        <a href="#"><img src="timeline/add2withe.png" onclick="entraIni()" style="width:25px;height:29px;margin-top: 55px;" id="new" title="Agregar un nuevo Riego" data-toggle='modal' data-target='#editarSensorTRB'></a>
        <div class="modal fade" id="editarSensorTRB" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header" style="background: linear-gradient(#3c3c3c,#111);">
                        <b style="color: white;font-family: Arial;">LemControl</b>
                        <button type="button" data-dismiss="modal" aria-hidden="true" style="float: right;background-color: rgba(255, 255, 255, 0);border: 0px;
                                color: white;margin-top: -2px;font-family: Arial;font-weight: 700;">X</button>
                    </div>
                    <div class="modal-body">
                        <div class="row" id="padremodal">
                            <div class="col-md-12">
                                <div class="col-md-2">
                                    <h5 class="visible-lg visible-md">Sectores de riego</h5>
                                </div>
                                <div class="col-md-3">
                                    <h5 class="visible-lg visible-md">Fecha de inicio riego</h5>
                                </div>
                                <div class="col-md-2">
                                    <h5 class="visible-lg visible-md">Tipo riego</h5>
                                </div>
                                <div class="col-md-2">
                                    <h5 class="visible-lg visible-md">Hora/m3</h5>
                                </div>
                                <div id="gradolabbel" class="col-md-1" style="visibility:visible;">
                                    <h5 class="visible-lg visible-md">Grados</h5>
                                </div>
                                <div class="col-md-1">
                                    <h5 class="visible-lg visible-md">Agregar</h5>
                                </div>
                                <div class="col-md-1">
                                    <h5 class="visible-lg visible-md">Eliminar</h5>
                                </div>
                            </div>
                            <div class="col-md-12" id="g_8">
                                <div class="col-md-2"> <select class="form-control" id="sectores_8" onchange="validartiemporeal(id);">
                                        <?php
                                        $b = 0;
                                        while ($b < $largo) {
                                            echo '<option  value="' . $b . '">' . $nombre_sector_control[$b] . ' </option>';
                                            $b = $b + 1;
                                        }
                                        ?>
                                    </select></div>
                                <div class="col-md-3">
                                    <div class='input-group date'>
                                        <input type='text' id="datetimepicker_8" onChange="fechamin1(id);
                                                validartiemporeal(id);" class="form-control" />
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <select class="form-control" onchange="selecciontipo(id);
                                            validartiemporeal(id);" id="tipos_8">
                                        <option value="2">Duracion</option>
                                        <!---<option  value="1">Fecha inicio</option>-->
                                        <option value="3">M3</option>
                                    </select>
                                </div>
                                <div class="col-md-3 " id="fechafin_8" style="display:none;">
                                    <div class='input-group date'>
                                        <input type='text' id='datetimepicker_108' onChange="validartiemporeal(id);" class="form-control" />
                                    </div>
                                </div>
                                <div class="col-md-2" style="display:block;" id="duracion_8" onChange="validartiemporeal(id);">
                                    <input id="duracionriego_8" value="00:00" type='time' class="form-control" />
                                </div>
                                <div class="col-md-1" style="visibility:hidden;" id="duracion_8" onChange="validartiemporeal(id);">
                                    <input id="grado_8" value="0" type='input' class="form-control" />
                                </div>
                                <div class="col-md-2" style="display:none;" id="volumen_8" onChange="validartiemporeal(id);">
                                    <input id="volumeninput_8" placeholder="m3" type="number" class="form-control" />
                                </div>
                                <div class="col-md-1"><a id="a_8" onclick=" validartiemporeal(id);
                                        agregarnuevoriego(id);" style="cursor:pointer;"><img class="visible-lg visible-md" src="imagenes/agregar.png" style="width:28px;"></a>
                                </div>
                                <div class="col-md-1"><a id="e_8" style="cursor:pointer;"><img class="visible-lg visible-md" src="imagenes/trash.png" style="width:30px;"></a></div>
                            </div>
                        </div>
                        <form class="form-horizontal">
                            <div id="fertil" style="display:none;">
                                <h5 style="text-align: left;">Agregar Fertirriego</h5>
                                <select onChange="obtenerFertil(value);" style="position: relative;margin-left: 176px;top: -32px;width: 209px;" class="form-control">
                                    <option>Seleccione</option>
                                    <?php
                                    $b = 0;
                                    while ($b < $largo2) {
                                        echo '<option  value="' . $b . '">' . $nombre_sector_control_ferti[$b] . ' </option>';
                                        $b = $b + 1;
                                    }
                                    ?>
                                </select>
                                <h5 style="text-align:left;">Fecha y Hora de inicio</h5>
                                <div class='input-group date' style="width: 208px;position: relative;left: 176px;top: -36px;">
                                    <input type='text' id='datetimepicker4' onChange="fechamin2();" class="form-control" />
                                </div>
                                <h5 style="text-align:left;">Tipo de Riego</h5>
                                <h6 style="position: relative;top: -25px;left: -157px;">Duración</h6><input id="duracionFerti" ;checked="checked" onclick="entra6();" type="radio" name="tipo2" value="6" style="position: relative;top: -51px;left: -123px;z-index:1;" />
                                <h6 style="position: relative;top: -72px;left: -41px;">Fecha y Hora Fin</h6><input onclick="entra4();" type="radio" name="tipo2" value="4" style="position: relative;top: -98px;left: 15px;" />
                                <!--<input onclick="entra5();" type="radio" name="tipo2" value="5" /> Volumen -->

                                <div class="form-group" style="display:none;" id="duracion2">
                                    <div>
                                        <h5 style="float: left;position: relative;left: 15px;top:-61px;">Duración</h5>
                                        <div class="col-sm-10" style="position: relative;top: -63px;left: 119px;">
                                            <div class='input-group date' id='datetimepicker_8' style="width:205px">
                                                <input id="duracionriegofertil" value="00:00" type='time' class="form-control" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group" style="display:none;" id="fechafin2">
                                    <h5 style="position: relative;left: -296px;top: -60px;">Fecha y Hora de Fin</h5>
                                    <div class="col-sm-10" style="width: 237px;float: right;top: -96px;left: -333px;">
                                        <div class='input-group date' style="width:205px">
                                            <input type='text' id='datetimepicker5' class="form-control" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group" style="display:none;" id="volumen2">
                                    <div>
                                        <div style="position: relative;left: -330px;">Volumen</div>
                                        <div class="col-sm-10">
                                            <div class='input-group date' style="width:200px">
                                                <input id="volumeninputFerti" placeholder="m3" type='text' class="form-control" style="position: relative;left: 139px;top: -24px;" />
                                            </div>
                                            <div class="checkbox" style="float: right;position: relative;left: 84px;top: -60px;">
                                                <label>
                                                    <input type="radio" name="prioridadFerti" value="1" />Maxima Prioridad<input type="radio" name="prioridadFerti" value="0" /> Minima Prioridad
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <h5 style="float: left;position: relative;margin-top: -1%;left: 2.3%;">Bomba Continua</h5>
                        <label class="switch" style="float: left;left: -7.5%;margin-top: 2%;">
                            <input type="checkbox" id="bomba_continua">
                            <span class="slider round"></span>
                        </label>   
                        <input style="margin:0px" type="button" value="Limpiar Riegos"  class="btn btn-danger" onclick="limpiar_riegos()"/>         
                        <input style="margin:0px" type="button" value="Cargar Plan"  class="btn btn-default" onclick="mostrar_modal_cargar_plan()"/>
                        <input style="margin:0px" type="button" value="Guardar Plan"  class="btn btn-default" onclick="mostrar_modal_guardar_plan()"/>
                        <input style="margin:0px" id="aceptar" disabled="disabled" type="button" value="Aceptar" onclick="pasaporte(1);" class="btn btn-default" data-dismiss="modal" />
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="mostrarlabel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content" style="width: 672px;">
                    <div class="modal-header" style="background: linear-gradient(#3c3c3c,#111);">
                        <b style="color: white;font-family: Arial;">Simbología de Riegos</b>
                        <button type="button" data-dismiss="modal" aria-hidden="true" style="
                                float: right;
                                background-color: rgba(255, 255, 255, 0);
                                border: 0px;
                                color: white;
                                margin-top: -2px;
                                font-family: Arial;
                                font-weight: 700;
                                ">X</button>
                    </div>

                    <div class="modal-footer">
                        <img src='imagenes/labelColor.jpg' style="width: 628px;position: relative;top: -2px;">
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="editarNodos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content" style="width: 672px;">
                    <div class="modal-header" style="background: linear-gradient(#3c3c3c,#111);">
                        <b style="color: white;font-family: Arial;">Reinicio de Equipos</b>
                        <button type="button" data-dismiss="modal" aria-hidden="true" style="
                                float: right;
                                background-color: rgba(255, 255, 255, 0);
                                border: 0px;
                                color: white;
                                margin-top: -2px;
                                font-family: Arial;
                                font-weight: 700;
                                ">X</button>
                    </div>
                    <div id="listaSectores" class="modal-body">
                        <select class="form-control" style="width: 158px;margin-left: 242px;" id="campos" onChange="modificar(value)">
                            <option>Campos</option>
                        </select>
                        <!--<input type="button" class="btn btn-warning" style="float: right;position: relative;top: -34px;left: -170px;" data-toggle="modal" data-target="#configsector" id="config' + h + '" onClick="configSectores()" type="submit" value="Config" />-->
                        <div id="EstadoNodos">

                        </div>
                        </br>
                    </div>
                    <div class="modal-footer">
                        <!--<input class="btn btn-primary" id="aceptar" type="button" value="Aceptar" onclick="pasaporte(2);" data-dismiss="modal"/>-->
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="descargarExcel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content" style="width: 672px;">
                    <div class="modal-header" style="
                         color: white;
                         background: linear-gradient(#3c3c3c,#111);
                         font-family: Arial;
                         font-weight: 700;
                         ">Excel de Riegos Agendados
                        <button type="button" data-dismiss="modal" aria-hidden="true" style="
                                float: right;
                                background-color: rgba(255, 255, 255, 0);
                                border: 0px;
                                color: white;
                                margin-top: -2px;
                                ">X</button></div>

                    <div id="listaSectores" class="modal-body" style="height: 200px;">
                        <div class="form-group">
                            <div style="float: left;position: relative;top: 36px;left: 8px;">Fecha y Hora de inicio:</div>
                            <div class="col-sm-10">
                                <div class='input-group date' style="width:200px;left: 156px;top: 12px;z-index: 3;">
                                    <input id="fechainiexcel" type='date' class="form-control" />
                                </div>
                            </div>
                            <div style="float: left;position: relative;top: 36px;left: 8px;">Fecha y Hora de fin:</div>
                            <div class="col-sm-10">
                                <div class='input-group date' style="width:200px;left: 156px;top: 12px;z-index: 3;">
                                    <input id="fechafinexcel" type='date' class="form-control" />
                                </div>
                            </div>
                            <div style="left: -202px;position: relative;">
                                Riegos del Equipo: &nbsp; &nbsp;<input type="radio" id="equipoExcel" name="equipo" value="1" checked="checked" /> &nbsp; &nbsp; &nbsp;
                                Riego de Todos los Equipos: &nbsp; &nbsp;<input type="radio" id="todosExcel" name="equipo" value="2" />
                            </div>
                            <form action="ajax/excelControl.php" method="Post" id="myForm5" data-ajax="false">
                                <div style="float: left;">
                                    <input type="text" name="id_empresa" id="empresaExcel" value="" style="display:none" />
                                    <input type="text" name="fecha_ini" id="fecha_iniExcel" value="" style="display:none" />
                                    <input type="text" name="fecha_fin" id="fecha_finExcel" value="" style="display:none" />
                                    <input type="text" name="id_equipo" id="id_equipoExcel" value="" style="display:none" />
                                    <input type="text" name="equipo" id="equipoExcel2" value="" style="display:none" />
                                    <div style="float: right; margin-left: 15px;">
                                        <input style="display:none" type="submit" style="float: none;font-size: initial;" class="classmenucerrar square green effect-2" data-inline="true" value="Consultar" />
                                    </div>
                            </form>
                            <form action="excelControlMontegrande.php" method="Post" id="myForm77" data-ajax="false">
                                <div style="float: left;">
                                    <input type="text" name="id_empresa1" id="empresaExcel1" value="" style="display:none" />
                                    <input type="text" name="fecha_ini1" id="fecha_iniExcel1" value="" style="display:none" />
                                    <input type="text" name="fecha_fin1" id="fecha_finExcel1" value="" style="display:none" />
                                    <input type="text" name="id_equipo1" id="id_equipoExcel1" value="" style="display:none" />
                                    <input type="text" name="equipo1" id="equipoExcel21" value="" style="display:none" />
                                    <div style="float: right; margin-left: 15px;">
                                        <input style="display:none" type="submit" style="float: none;font-size: initial;" class="classmenucerrar square green effect-2" data-inline="true" value="Consultar" />
                                    </div>
                            </form>



                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <input class="btn btn-default" id="aceptar" type="button" value="Descargar" onclick="excel();" data-dismiss="modal" />
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="configsector" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content" style="width: 672px;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Configuracion del Sector </h4>
                </div>
                <div class="modal-body">
                    <form>
                        <label for="exampleInputEmail2" style="left: -204px;position: relative;">Caudal Teorico del Sector:</label></br>
                        <div class="form-inline">
                            <label for="caudalInferior">Umbral Inferior</label>
                            <input type="text" class="form-control" id="caudalInferior">
                            <label for="caudalSuperior">Umbral Superior</label>
                            <input type="email" class="form-control" id="caudalSuperior"></br></br>
                        </div>
                        <div class="form-inline">
                            <label for="exampleInputEmail2" style="left: -198px;position: relative;">Margen de Error de Tiempo:</label></br>
                            <label for="tiempoInferior">Umbral Inferior</label>
                            <input type="text" class="form-control" id="tiempoInferior">
                            <label for="tiempoSuperior">Umbral Superior</label>
                            <input type="email" class="form-control" id="tiempoSuperior"></br></br>
                        </div>
                        <div class="form-inline">
                            <label for="exampleInputEmail2" style="left: -203px;position: relative;">Prescion Teorica del Sector:</label></br>
                            <label for="prescionInferior">Umbral Inferior</label>
                            <input type="text" class="form-control" id="prescionInferior">
                            <label for="prescionSuperior">Umbral Superior</label>
                            <input type="email" class="form-control" id="prescionSuperior"></br></br>
                        </div>

                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
    </div>
    <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
        <div class="modal-dialog modal-sm" role="document" style="top: 41%;background:  white;height: 28px;padding: 3px;border-radius: 8px;">
            <div class="modal-content">
                <div class="bar"></div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div id="modal_riegos_estados" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Estado de agendamiento</h4>
                </div>
                <div class="modal-body">
                    <table class="table" order="1">
                        <thead>
                            <tr>
                                <th>Sector</th>
                                <th>Fecha inicio</th>
                                <th>Fecha fin</th>
                                <th>Estado</th>
                            </tr>
                        </thead>
                        <tbody id="cuerpo_tabla_riegos">
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button onClick="window.location.reload();" type="button" class="btn btn-success" data-dismiss="modal">Aceptar</button>
                </div>
            </div>

        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="estado_internet" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Fallo conexión sistema local</h4>
                </div>
                <div class="modal-body">
                    <p>Utilice el servidor local</p>
                    <img src="imagenes/net_img.PNG">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">cerrar</button>
                </div>
            </div>

        </div>
    </div>
    <!-- Modal 2 -->
    <div class="modal fade" id="traspaso_riegos" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Traspaso de información</h4>
                </div>
                <div class="modal-body">
                    <p>Traspasando riegos hacia servidor Maestro</p>
                    <img src="imagenes/net_img_paso3.gif">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">cerrar</button>
                </div>
            </div>

        </div>
    </div>
    <!-- Modal 3 -->
    <div class="modal fade" id="modal_manual" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Modo Manual</h4>
                </div>
                <div class="modal-body">
                    <p>En estos momentos esta en modo manual, favor dirigirse a la caseta</p>
                    <img src="imagenes/manual1.gif">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">cerrar</button>
                </div>
            </div>

        </div>
    </div>
    <!-- Modal 4 Guardar Plan Riegos -->
    <div class="modal fade" id="plan_riegos" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Guardar Plan</h4>
                </div>
                <div class="modal-body">
                <form class="form-inline">
                    <div>
                        <label for="exampleInputName2" style="float:left;margin-top: 1.3%;">Nombre:</label>
                        <input type="text" style="margin-left: -3%;width: 84%;" class="form-control" id="nombre_planificacion_txt" placeholder="Plan 1">
                    </div>
                </form>
                </div>
                <div class="modal-footer">
                    <button onclick="guardar_planificacion()" type="button" class="btn btn-default" data-dismiss="modal">Guardar</button>
                </div>
            </div>

        </div>
    </div>    
    <!-- Modal 5 cargar Plan Riegos -->
    <div class="modal fade" id="cargar_plan_riegos" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content" style="max-height: 67%;">
                <div class="modal-header" style="background: linear-gradient(#3c3c3c,#111);height: 7%;">
                    <button style="color:white" type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" style="color:white">Seleccionar Plan</h4>
                </div>
                <div class="modal-body" style="min-height:50%;max-height: 50%;">
                <form>
                    <div class="row" style="padding: 10px;">
                        <div class="col-md-12>">
                            <label style="float: left;"for="exampleInputEmail1">Programa de Riego:</label>
                            <select id="select_planificaciones" class="form-control">
                                <option value="0" disabled selected>Seleccione un Programa:</option>
                            </select>        
                        </div> 
                    </div>
                    <div class="row" style="padding: 10px;">
                        <div class="col-md-6>">
                            <label style="float: left;"for="exampleInputEmail1">Tipo de Repeticion del Programa de Riego:</label>
                            <select id="select_tipo_planificacion" class="form-control" onchange="setFechaInicio(value)">
                                <option value="0" disabled selected>Seleccione Tipo de Programa:</option>
                                <option value="1">Programa se replica semanalmente</option>
                                <option value="2">Programa se replica diariamente</option>
                            </select>        
                        </div> 
                    </div>      
                    <div class="row" style="padding: 10px;">
                        <div class="col-md-6>">
                            <label style="float: left;"for="exampleInputEmail1">Fecha Inicio del Programa de Riego:</label>
                            <input onchange="setFechaFin(value)" class="form-control" id="fechaIniPlanificacion" type="date">        
                        </div> 
                    </div>
                    <div class="row" style="padding: 10px;">
                        <div class="col-md-6>">
                            <label style="float: left;"for="repeticion_programacion">Cantidad de Repeticiones que tendr el programa de Riego:</label>
                            <input value="1" class="form-control" id="repeticion_programacion"type="number">        
                        </div> 
                    </div>                              
                </form>
                </div>
                <div class="modal-footer">   
                    <button style="float: left;" onclick="eliminarPrograma()" type="button" class="btn btn-danger">Eliminar Programa</button>
                    <button onclick="previsualizar()" onclick="" type="button" class="btn btn-primary">Pre Visualiar</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal 6 cargar Previsualizacion Plan Riegos -->
    <div class="modal fade" id="vizualizar_plan_riego" role="dialog" >
        <div class="modal-dialog" style="height: 90%;width: 90%;" > 
            <!-- Modal content-->
            <div class="modal-content" style="width: 100%;height: 100%;">
                <div class="modal-header" style="background: linear-gradient(#3c3c3c,#111);height: 7%;">
                    <button style="color:white" type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" style="color:white">Visualizacion Programa de Riego</h4>
                </div>
                <div class="modal-body" style="min-height:50%;height: 80%;">
                <div id="mytimeline3"  style="height: 100%;width: 100%;position: absolute;display: block;z-index: 0;left: 0px;top: 0px;"></div>
                </div>
                <div class="modal-footer">
                    <button onclick="cargarPrograma()" type="button" class="btn btn-default" >Cargar Programa</button>
                </div>
            </div>
        </div>
    </div>
    </div>
    <script>
        var userAgent = navigator.userAgent || navigator.vendor || window.opera;
        var celular = <?php echo $celular; ?>;
        var click_celular = 0;
        var nombreP = <?php echo json_encode($nombreP); ?>;
        var p_fecha_iniP = <?php echo json_encode($p_fecha_iniP); ?>;
        var p_fecha_finP = <?php echo json_encode($p_fecha_finP); ?>;
        var r_fecha_iniP = <?php echo json_encode($r_fecha_iniP); ?>;
        var r_fecha_finP = <?php echo json_encode($r_fecha_finP); ?>;
        var estadoP = <?php echo json_encode($estadoP); ?>;
        var idP = <?php echo json_encode($idP); ?>;
        var unidadCaudal = <?php echo json_encode($unidadCaudal); ?>;

        var riegos;
        var riegoyferti = 0;
        var repeticion = "";
        var swValvulas = <?php echo json_encode($swValvulas); ?>;
        var fechadatosensorcaudal = <?php echo json_encode($fechadatosensorcaudal); ?>;
        var descripcionCaudal = <?php echo json_encode($descripcionCaudal); ?>;
        var datosensorcaudal = <?php echo json_encode($datosensorcaudal); ?>;
        var equipo_post = <?php echo json_encode($equipo_post); ?>;
        var caseta_post = <?php echo json_encode($caseta_post); ?>;
        var equipo = <?php echo json_encode($equipo); ?>;
        var totalsubriego = 0;
        var riegosManuales = <?php echo $riegosManuales; ?>;
        var lostconnection = <?php echo $lostconnection; ?>;
        var id_nodoTotal = <?php echo json_encode($id_nodoTotal); ?>;
        var estadonodoTotal = <?php echo json_encode($estadonodoTotal); ?>;
        var nombre_sector3 = <?php echo json_encode($nombre_sector3); ?>;
        var id_sectorTotal = <?php echo json_encode($id_sectorTotal); ?>;
        var nombre_sector_control_ferti = <?php echo json_encode($nombre_sector_control_ferti); ?>;
        var nombre_sector_control = <?php echo json_encode($nombre_sector_control); ?>;
        var tipoRiego = <?php echo json_encode($tipoRiego); ?>;
        var names = <?php echo json_encode($nombre_sector); ?>;
        var id_sector_ferti = <?php echo json_encode($id_sector_ferti); ?>;
        var id_sector = <?php echo json_encode($id_sector); ?>;
        var p_fecha_ini = <?php echo json_encode($p_fecha_ini); ?>;
        var p_fecha_fin = <?php echo json_encode($p_fecha_fin); ?>;
        var r_fecha_ini = <?php echo json_encode($r_fecha_ini); ?>;
        var r_fecha_fin = <?php echo json_encode($r_fecha_fin); ?>;
        var estado = <?php echo json_encode($estado); ?>;
        var tipo = <?php echo json_encode($tipo); ?>;
        var update_p = <?php echo json_encode($update_p); ?>;
        var id_nodo = <?php echo json_encode($id_nodo); ?>;
        var estadonodo = <?php echo json_encode($estadonodo); ?>;
        var subriegoIni = <?php echo json_encode($subriegoIni); ?>;
        var subriegoFin = <?php echo json_encode($subriegoFin); ?>;
        var subriegoTipo = <?php echo json_encode($subriegoTipo); ?>;
        var estadoSubriego = <?php echo json_encode($estadoSubriego); ?>;
        var minT = <?php echo json_encode($minT); ?>;
        var id = <?php echo json_encode($id); ?>;
        var timeline = undefined;
        var data = undefined;
        var id_sectorControl = 0;
        var id_sectorFertil = 0;
        var nombre;
        var nombreFertil;
        var divsubriego = "";
        var id_empresa = <?php echo $id_empresa; ?>;
        var sensorescaudal = <?php echo $sensorescaudal; ?>;
        var nombre_tabla_muestra_mensual = <?php echo json_encode($nombre_tabla_muestra_mensual) ?>;
        var cantidadderiegos = 8;
        var chart = '';
        var timeline2 = undefined;
        var swCaudal = 0;
        var restart_fecha = '';
        var reend_fecha = '';
        var graficos_amchart = undefined;
        var swZoom = 0;
        var swPresostatos = 0;
        var div_ancho_nombre_sectores = 0;
        var ancho_caudal = 0;
        var actualizarNodos = null;
        repeticion2 = window.setInterval("actualizarRiegos()", 120000); //Cambiar a 120000

        var puedoagregar = "si";

        function borrarRiegos() {
            $("#info").remove();
            var options = {
                width: "100%",
                height: "80%",
                layout: "box",
                axisOnTop: true,
                eventMargin: 10, // minimal margin between events
                eventMarginAxis: 0, // minimal margin beteen events and the axis
                editable: false,
                showNavigation: true,
                selectable: true,
                locale: 'es',
                showMajorLabels: true,
                groupMinHeight: 20,
                groupsOnRight: false,
                zoomable: true,
                stackEvents: false,
                groupsChangeable: true,
                zoomMin: 86400000,
                zoomMax: 604800000,
                groupsWidth: "16.4%"
            };
            timeline = new links.Timeline(document.getElementById('mytimeline'), options);
            timeline.deleteAllItems();

            var options = {
                width: "100%",
                height: "80%",
                layout: "box",
                axisOnTop: true,
                eventMargin: 10, // minimal margin between events
                eventMarginAxis: 0, // minimal margin beteen events and the axis
                editable: false,
                showNavigation: true,
                selectable: true,
                locale: 'es',
                showMajorLabels: true,
                groupMinHeight: 20,
                groupsOnRight: false,
                zoomable: true,
                stackEvents: false,
                groupsChangeable: true,
                zoomMin: 86400000,
                zoomMax: 604800000
            };
            timeline2 = new links.Timeline(document.getElementById('mytimeline2'), options);
            timeline2.deleteAllItems();
        }
        google.load("visualization", "1");
        // Set callback to run when API is loaded
        google.setOnLoadCallback(drawVisualization);

        function ajustar_chartline() {
            var ancho_pantalla = $(window).width();
            var left_caudal = (div_ancho_nombre_sectores * 100) / ancho_pantalla;
            ancho_caudal = 100 - left_caudal;
            left_caudal = left_caudal + "%";
            ancho_caudal = ancho_caudal + "%";
            $("#chartdiv").css("width", ancho_caudal);
            $("#chartdiv").css("left", left_caudal);
            graficoCaudal();
        }
        function onRangeChanged(properties) {
            var i = moment(properties.start);
            var f = moment(properties.end);

            restart_fecha = i;
            reend_fecha = f;

            if (swValvulas != 0) {

            }
            if (datosensorcaudal[0] != -1) {
                chart.zoomToDates(properties.start, properties.end);
            }
            timeline.setVisibleChartRange(properties.start, properties.end);
            timeline2.setVisibleChartRange(properties.start, properties.end);
        }
        function sortJSON(data, key, orden) {
            return data.sort(function(a, b) {
                var x = a[key],
                    y = b[key];

                if (orden === 'asc') {
                    return ((x < y) ? -1 : ((x > y) ? 1 : 0));
                }

                if (orden === 'desc') {
                    return ((x > y) ? -1 : ((x < y) ? 1 : 0));
                }
            });
        }
        function timelineValvula() {
            //ajax Actualizar Riegos Programado
            var data = 'equipo_post=' + equipo_post + '&id_empresa=' + id_empresa;
            $.ajax({
                url: 'ajax/actualizarValvulas.php',
                type: 'post',
                data: data,
                beforeSend: function() {},
                success: function(resp) {
                    if (swValvulas != 0) {
                        var valvulas = JSON.parse(resp);
                        data3 = new google.visualization.DataTable();
                        data3.addColumn('datetime', 'start');
                        data3.addColumn('datetime', 'end');
                        data3.addColumn('string', 'content');
                        data3.addColumn('string', 'group');
                        data3.addColumn('string', 'className');
                        for (var n = 0, len = valvulas[0].numero_filas; n < len; n++) {
                            var id_programacion_divP = valvulas[n].id_programacion_riego;
                            var est = valvulas[n].estado;
                            var name = valvulas[n].nombre;
                            var now = new Date();
                            var end = new Date();
                            if (est == 7 || est == 4 || est == 6 || est == 8) {
                                var end = new Date(valvulas[n].r_fecha_fin);
                            } else {
                                var end = new Date(valvulas[n].p_fecha_fin);
                            }


                            var start = new Date(valvulas[n].p_fecha_ini);
                            var duracion = end - start;
                            duracion = (((duracion / 1000) / 60) / 60);


                            var start2 = new Date(valvulas[n].p_fecha_ini);
                            var end2 = new Date(valvulas[n].r_fecha_fin);
                            var duracion2 = end2 - start2;
                            duracion2 = (((duracion2 / 1000) / 60) / 60);
                            ancho = duracion2 * 100;
                            ancho = ancho / duracion;


                            var start3 = new Date(valvulas[n].r_fecha_ini);
                            var end3 = new Date(valvulas[n].r_fecha_fin);
                            var duracion3 = end3 - start3;
                            duracion3 = (((duracion3 / 1000) / 60) / 60);
                            ancho2 = duracion3 * 100;
                            ancho2 = ancho2 / duracion;
                            var left = ancho - ancho2;
                            //var ini = p_fecha_ini[n].split(" ");
                            //var fin = p_fecha_fin[n].split(" ");
                            var imagen = "";
                            var cargando_p = " ";
                            if (est == 4) {
                                var color = 'rgb(113, 113, 102)';
                            }
                            if (est == 2) {
                                var color = 'green';
                            }
                            if (est == 1 || est == 33) {
                                var color = 'rgb(24, 85, 155)';
                            }
                            if (est == 3 || est == 7 || est == 8) {
                                var color = 'rgb(184, 23, 23)';
                                left = '0px';
                                ancho2 = '30px';
                            }
                            if (est == 6) {
                                var color = 'green; background: repeating-linear-gradient(45deg,  #606dbc,  #606dbc 10px,  #465298 10px,  #465298 20px)';

                            }
                            if (est == 5) {
                                var color = 'red';
                            }
                            if (est == 0) {
                                var color = '#5D99C3';
                            }

                            var availability = '<div style="border-width:4px;"  onMouseOut=borrarinfo();>' + cargando_p + '<div onMouseOut=borrarinfo(); id="' + id_programacion_divP + '" style=" width: ' + ancho2 + '%; margin-left:' + left + '%; background-color: ' + color + '; "id="i' + n + '";><div  onMouseOut=borrarinfo(); style="color: #5D99C3;">&nbsp;</div></div>' + imagen + '</div>' + divsubriego + '';
                            var group = availability.toLowerCase();
                            var content = availability;
                            data3.addRow([start, end, content, name, group]);
                            divsubriego = "";
                        }
                        div_ancho_nombre_valvulas = div_ancho_nombre_sectores + "px";

                        // specify options 
                        var options = {
                            width: "100%",
                            eventMarginAxis: 0,
                            height: "auto",
                            layout: "box",
                            axisOnTop: true,
                            eventMargin: 10, // minimal margin between events
                            eventMarginAxis: 0, // minimal margin beteen events and the axis
                            editable: false,
                            showNavigation: true,
                            selectable: true,
                            locale: 'es',
                            showMajorLabels: true,
                            groupMinHeight: 20,
                            groupsOnRight: false,
                            zoomable: false,
                            stackEvents: false,
                            groupsChangeable: true,
                            zoomMin: 86400000,
                            zoomMax: 604800000,
                            groupsWidth: div_ancho_nombre_valvulas
                        };

                        // Instantiate our timeline object.
                        timeline2 = new links.Timeline(document.getElementById('mytimeline2'), options);

                        // attach an event listener using the links events handler
                        links.events.addListener(timeline2, 'rangechanged', onRangeChanged);
                        // register event listeners
                        google.visualization.events.addListener(timeline2, 'edit', onEdit);

                        // Draw our timeline with the created data and options
                        timeline2.draw(data3);
                        if (typeof(restart_fecha) == "undefined") {
                            restart_fecha = new Date(now.getTime() - 12 * 60 * 60 * 1000);
                        }
                        if (typeof(reend_fecha) == "undefined") {
                            reend_fecha = new Date(now.getTime() + 12 * 60 * 60 * 1000);
                        }
                        var start = new Date(restart_fecha);
                        var end = new Date(reend_fecha);
                        timeline2.setVisibleChartRange(start, end);


                    }
                }
            })
        }
        function timelineSectores() {
            //ajax Actualizar Riegos Programados
            var data = 'equipo_post=' + equipo_post + '&id_empresa=' + id_empresa;
            $.ajax({
                url: 'ajax/actualizarRiegos.php',
                type: 'post',
                data: data,
                async: false,
                beforeSend: function() {},
                success: function(resp) {
                    console.log(resp);
                    var riegos = JSON.parse(resp);
                    var id_programacion_div = riegos[0].id_programacion_riego;
                    // Create and populate a data table.
                    data = new google.visualization.DataTable();
                    data.addColumn('datetime', 'start');
                    data.addColumn('datetime', 'end');
                    data.addColumn('string', 'content');
                    data.addColumn('string', 'group');
                    data.addColumn('string', 'className');
                    // create some random data
                    for (var n = 0, len = riegos.length; n < len; n++) {
                        id[n] = riegos[n].id_programacion_riego;
                        minT[n] = riegos[n].minT;
                        p_fecha_ini[n] = riegos[n].p_fecha_ini;
                        p_fecha_fin[n] = riegos[n].p_fecha_fin;
                        r_fecha_ini[n] = riegos[n].p_fecha_ini;
                        r_fecha_fin[n] = riegos[n].p_fecha_fin;
                        var id_programacion_div = riegos[n].id_programacion_riego;
                        var est = riegos[n].estado;
                        var up_p = riegos[n].update_p;
                        var name = riegos[n].sector;
                        var now = new Date();
                        var end = new Date();
                        if (est == 7 || est == 4 || est == 6 || est == 8) {
                            var end = new Date(riegos[n].r_fecha_fin);
                        } else {
                            var end = new Date(riegos[n].p_fecha_fin);
                        }
                        if (riegos[n].numero_filas != 0) {
                            for (var y = 0, len1 = riegos[n].numero_filas; y < len1; y++) {
                                var ini = new Date(riegos[n].p_fecha_ini);
                                var fin = new Date(riegos[n][y].p_fecha_ini);
                                var estadoSub = riegos[n][y].estado;
                                if (estadoSub == 4) {
                                    var color = 'rgb(113, 113, 102)';
                                }
                                if (estadoSub == 2) {
                                    var color = 'green';
                                }
                                if (estadoSub == 1 || estadoSub == 33) {
                                    var color = 'rgb(24, 85, 155)';
                                }
                                if (estadoSub == 3 || est == 7 || est == 8) {
                                    var color = 'rgb(184, 23, 23)';
                                    left = '0px';
                                    ancho2 = '30px';
                                }
                                if (estadoSub == 6) {
                                    var color = 'green; background: repeating-linear-gradient(45deg,  #606dbc,  #606dbc 10px,  #465298 10px,  #465298 20px)';
                                }
                                if (estadoSub == 5) {
                                    var color = 'red';
                                }
                                if (estadoSub == 0) {
                                    var color = '#5D99C3';
                                }
                                var x = fin - ini;
                                var pini = new Date(riegos[n].p_fecha_ini);
                                var pfin = new Date(riegos[n].p_fecha_fin);
                                var z = pfin - pini;
                                var subleft = x * 100;
                                var subleft = subleft / z;
                                var subleft2 = subleft + 20;
                                var ini2 = new Date(riegos[n][y].p_fecha_ini);
                                var fin2 = new Date(riegos[n][y].p_fecha_fin);
                                var x = fin2 - ini2;
                                var sublargo = x * 100;
                                var sublargo = sublargo / z;
                                var divsubriego = divsubriego + '<div onMouseOver=creainfo("' + n + '",event); onMouseOut=borrarinfo(); style="position: absolute; top:0px; width: ' + sublargo + '%; margin-left:' + subleft + '%; background-color: ' + color + '; "id="i' + n + '";><div onMouseOver=creainfo("' + n + '",event); onMouseOut=borrarinfo(); style="color: #5D99C3;">&nbsp;</div></div>';
                            }
                        }
                        var start = new Date(riegos[n].p_fecha_ini);
                        var duracion = end - start;
                        duracion = (((duracion / 1000) / 60) / 60);
                        var start2 = new Date(riegos[n].p_fecha_ini);
                        var end2 = new Date(riegos[n].r_fecha_fin);
                        var duracion2 = end2 - start2;
                        duracion2 = (((duracion2 / 1000) / 60) / 60);
                        ancho = duracion2 * 100;
                        ancho = ancho / duracion;
                        var start3 = new Date(riegos[n].r_fecha_ini);
                        var end3 = new Date(riegos[n].r_fecha_fin);
                        var duracion3 = end3 - start3;
                        duracion3 = (((duracion3 / 1000) / 60) / 60);
                        ancho2 = duracion3 * 100;
                        ancho2 = ancho2 / duracion;
                        var left = ancho - ancho2;
                        var ini = riegos[n].p_fecha_ini.split(" ");
                        var fin = riegos[n].p_fecha_fin.split(" ");
                        var imagen = "";
                        var cargando_p = " ";
                        if (est == 4) {
                            var color = 'rgb(113, 113, 102)';
                        }
                        if (est == 2) {
                            var color = 'green';
                        }
                        if (est == 1 || est == 33) {
                            var color = 'rgb(24, 85, 155)';
                        }
                        if (est == 12) {
                            var color = 'rgb(24, 85, 155)';
                            var cargando_p = "<img src='imagenes/pausa_amarillo.png' style='width: 18px;position: relative;top: -17px;'>"
                        }
                        if (est == 3 || est == 7 || est == 8) {
                            var color = 'rgb(184, 23, 23)';
                            left = '0px';
                            ancho2 = '30px';
                        }
                        if (est == 6) {
                            var color = 'green; background: repeating-linear-gradient(45deg,  #606dbc,  #606dbc 10px,  #465298 10px,  #465298 20px)';
                        }
                        if (est == 5) {
                            var color = 'red';
                        }
                        if (est == 0) {
                            if (up_p == 0) {
                                var cargando_p = "<img src='imagenes/loading.gif' style='width: 18px;position: relative;top: -2px;'>"
                            }
                            var color = '#5D99C3';
                        }
                        if (est == 12) {
                            var availability = '<div style="border-width:4px;" onMouseOver=creainfo("' + n + '",event); onMouseOut=borrarinfo();><div onMouseOver=creainfo("' + n + '",event); onMouseOut=borrarinfo(); id="' + id_programacion_div + '" style=" width: ' + ancho2 + '%; margin-left:' + left + '%; background-color: ' + color + '; height:17px" id="i' + n + '";><div onMouseOver=creainfo("' + n + '",event); onMouseOut=borrarinfo(); style="color: #5D99C3;">&nbsp;</div></div>' + imagen + '</div>' + divsubriego + '' + cargando_p + '';
                        } else {
                            var availability = '<div style="border-width:4px;" onMouseOver=creainfo("' + n + '",event); onMouseOut=borrarinfo();>' + cargando_p + '<div onMouseOver=creainfo("' + n + '",event); onMouseOut=borrarinfo(); id="' + id_programacion_div + '" style=" width: ' + ancho2 + '%; margin-left:' + left + '%; background-color: ' + color + '; "id="i' + n + '";><div onMouseOver=creainfo("' + n + '",event); onMouseOut=borrarinfo(); style="color: #5D99C3;">&nbsp;</div></div>' + imagen + '</div>' + divsubriego + '';
                        }
                        var group = availability.toLowerCase();
                        var content = availability;
                        data.addRow([start, end, content, name, group]);
                        divsubriego = "";
                    }
                    // specify options
                    // specify options
                    var options = {
                        width: "100%",
                        eventMarginAxis: 0,
                        height: "100%",
                        layout: "box",
                        axisOnTop: true,
                        eventMargin: 10, // minimal margin between events
                        eventMarginAxis: 0, // minimal margin beteen events and the axis
                        editable: false,
                        showNavigation: true,
                        selectable: true,
                        locale: 'es',
                        showMajorLabels: true,
                        groupMinHeight: 20,
                        groupsOnRight: false,
                        zoomable: true,
                        stackEvents: false,
                        groupsChangeable: true,
                        zoomMin: 86400000,
                        zoomMax: 604800000
                    };
                    if (id_empresa == 95 || id_empresa == 129) {
                        var options = {
                            width: "100%",
                            eventMarginAxis: 0,
                            height: "100%",
                            layout: "box",
                            axisOnTop: true,
                            eventMargin: 10, // minimal margin between events
                            eventMarginAxis: 0, // minimal margin beteen events and the axis
                            editable: false,
                            showNavigation: true,
                            selectable: true,
                            locale: 'es',
                            showMajorLabels: true,
                            groupMinHeight: 20,
                            groupsOnRight: false,
                            zoomable: true,
                            stackEvents: false,
                            groupsChangeable: true,
                            zoomMin: 86400000,
                            zoomMax: 604800000,
                            groupsWidth: '150px'
                        };
                    }
                    // Instantiate our timeline object.
                    timeline = new links.Timeline(document.getElementById('mytimeline'), options);
                    timeline = new links.Timeline(document.getElementById('mytimeline'), options);
                    // attach an event listener using the links events handler
                    links.events.addListener(timeline, 'rangechanged', onRangeChanged);
                    // register event listeners
                    google.visualization.events.addListener(timeline, 'edit', onEdit);
                    // Draw our timeline with the created data and options
                    timeline.draw(data);
                    if (typeof(restart_fecha) == "undefined") {
                        restart_fecha = new Date(now.getTime() - 12 * 60 * 60 * 1000);
                    }
                    if (typeof(reend_fecha) == "undefined") {
                        reend_fecha = new Date(now.getTime() + 12 * 60 * 60 * 1000);
                    }
                    var start = new Date(restart_fecha);
                    var end = new Date(reend_fecha);
                    timeline.setVisibleChartRange(start, end);
                }
            });
            div_ancho_nombre_sectores = $("div.timeline-groups-axis").width();
            if (id_empresa == 95 || id_empresa == 129) {
                div_ancho_nombre_sectores = 150;
            }
        }
        function graficoCaudal() {
            var id_empresa = <?php echo $id_empresa; ?>;
            var equipo = <?php echo json_encode($equipo_post); ?>;
            var data = 'equipo=' + equipo + '&id_empresa=' + id_empresa;
            $.ajax({
                url: 'ajax/graphCaudalimetro.php',
                type: 'post',
                data: data,
                async: false,
                beforeSend: function() {
                    console.log('enviando datos a la bd....')
                },
                success: function(resp) {
                    console.log(resp);
                    graficos_amchart = JSON.parse(resp);
                }
            });

            var data = 'equipo=' + equipo + '&id_empresa=' + id_empresa;
            $.ajax({
                url: 'ajax/actualizarCaudalimetro.php',
                type: 'post',
                data: data,
                async: false,
                beforeSend: function() {
                    console.log('enviando datos a la bd....')
                },
                success: function(resp) {
                    chartData = JSON.parse(resp);
                    chartData = sortJSON(chartData, 'date', 'asc');
                    console.log(chartData);
                    chart = AmCharts.makeChart("chartdiv", {
                        "type": "serial",
                        "theme": "light",
                        "marginRight": 0,
                        "dataProvider": chartData,
                        "mouseWheelZoomEnabled": false,
                        "legend": {
                            "useGraphSettings": true
                        },
                        "valueAxes": [{
                            "axisAlpha": 0.1,
                            "inside": true
                        }],

                        "graphs": graficos_amchart,
                        "zoomOutButtonRollOverAlpha": 0.15,
                        "chartCursor": {
                            "valueLineBalloonEnabled": true,
                            "cursorAlpha": 1,
                            "cursorColor": "#258cbb",
                            "valueLineAlpha": 0.2,
                            "valueZoomable": true
                        },
                        "autoMarginOffset": 5,
                        "columnWidth": 1,
                        "categoryField": "date",
                        "categoryAxis": {
                            "minPeriod": "mm",
                            "parseDates": true,
                            //"equalSpacing" : false
                            //"gridThickness": 0 
                        },
                        "export": {
                            "enabled": true
                        }
                    });
                    var now = new Date();
                    i = new Date(now.getTime() - 12 * 60 * 60 * 1000);
                    f = new Date(now.getTime() + 12 * 60 * 60 * 1000);




                    if (swCaudal == 0) {
                        chart.zoomToDates(i, f);
                        swCaudal = 1;
                    } else {
                        var i = new Date(restart_fecha);
                        i.setHours(i.getHours());
                        i.setMinutes(i.getMinutes());

                        var f = new Date(reend_fecha);
                        f.setHours(f.getHours());
                        f.setMinutes(f.getMinutes());
                        chart.zoomToDates(i, f);
                    }
                }
            });
        }
        function drawVisualization() {
            //Grafico Caudal	
            timelineSectores();
            ajustar_chartline();
            graficoCaudal();
            if (swValvulas != 0) {
                timelineValvula();
            }

        }
        function alerta_icono() {
            src_alerta = $('#alerta').attr('src');
            if (src_alerta == "imagenes/alerta_yellow.png") {
                $("#alerta").attr("src", "imagenes/alerta_red.png");
            } else {
                $("#alerta").attr("src", "imagenes/alerta_yellow.png");
            }
        }
        function actualizarRiegos() {
            var id_empresa = <?php echo $id_empresa; ?>;
            var data = 'id_empresa=' + id_empresa + '&equipo=' + equipo_post;
            var ping_net = 0;
            var lemtransmisor = false;
            $.ajax({
                url: 'ajax/transmisor.php',
                type: 'post',
                data: data,
                async: false,
                beforeSend: function() {},
                success: function(resp) {
                    console.log(resp);
                    if (resp == 0) {
                        document.getElementById("new").style.display = "block";
                        lemtransmisor = true;
                    }
                }
            });
            var data = 'id_empresa=' + id_empresa;
            date = "";
            $("#lista2").empty();
            $.ajax({
                url: 'ajax/alertasTest.php',
                type: 'post',
                data: data,
                beforeSend: function() {},
                success: function(resp) {

                    var alertas = JSON.parse(resp);
                    if (alertas != null) {
                        if ((!alertas[0].numero_filas == 0)) {
                            clearInterval(alertas_repite);
                            alertas_repite = window.setInterval("alerta_icono()", 500); //Cambiar a 120000
                        } else {
                            clearInterval(alertas_repite);
                        }
                        for (var i = 0; i < alertas[0].numero_filas; i++) {
                            tipo = alertas[i].tipo;
                            fecha = alertas[i].fecha;
                            nombre = alertas[i].nombre;
                            nombre_equipo = alertas[i].nombre_equipo;
                            if (tipo == 1) {
                                tipo = "Valvula no abre";
                            } else {
                                tipo = "Valvula abierta";
                            }
                            $("#lista2").append("<li style='text-align:left;'><strong>Equipo:</strong>" + nombre_equipo + "</br><strong>Nombre:</strong>" + nombre + "</br><strong>Fecha:</strong>" + fecha + "</br> < strong > Tipo: </strong>" + tipo + "<img src='alerta_valvula_ico.png' style='position: relative;float: right;width: 45px;top: -37px;left: -4px;'> < /li>");
                        }
                    } else {
                        clearInterval(alertas_repite);
                    }
                }
            });
            date = "";

            var id_empresa = <?php echo $id_empresa; ?>;
            var data = 'id_empresa=' + id_empresa;
            $.ajax({
                url: 'ajax/alertasTestCaudal.php',
                type: 'post',
                data: data,
                beforeSend: function() {},
                success: function(resp) {
                    var alertas = JSON.parse(resp);
                    if (alertas != null) {
                        if (!(alertas[0].numero_filas == 0)) {
                            clearInterval(alertas_repite);
                            alertas_repite = window.setInterval("alerta_icono()", 500); //Cambiar a 120000
                        } else {
                            clearInterval(alertas_repite);
                        }
                        for (var i = 0; i < alertas[0].numero_filas; i++) {

                            tipo = alertas[i].tipo;
                            fecha = alertas[i].fecha_ini;
                            nombre = alertas[i].nombre_sector;
                            nombre_equipo = alertas[i].nombre_equipo;
                            if (tipo == 'ci') {
                                tipo = "Bajo caudal";
                            } else {
                                tipo = "Sobre caudal";
                            }
                            $("#lista2").append("<li style='text-align:left;'><strong>Equipo:</strong>" + nombre_equipo + "</br><strong>Nombre:</strong>" + nombre + "</br><strong>Fecha:</strong>" + fecha + "</br><strong>Tipo:</strong>" + tipo + "<img src='alerta_caudal_ico.png' style='position: relative;float: right;width: 45px;top: -37px;left: -4px;'></li>");
                        }
                    } else {
                        clearInterval(alertas_repite);
                    }
                }
            });
            timelineSectores();
            timelineValvula();
            //Actualizar Timeline Riegos//
            if (!(typeof(sensorescaudal) == 'undefined')) {
                graficoCaudal();
            }
        }
        google.setOnLoadCallback(drawVisualization);
        function setTime() {
            var fecha = $('#datetimepicker6').val();
            var fechaini = fecha + " 00:00:00"
            var fechafin = fecha + " 23:59:59"
            var fechaini = new Date(fechaini);
            var fechafin = new Date(fechafin);
            timeline2.setVisibleChartRange(fechaini, fechafin);
            timeline.setVisibleChartRange(fechaini, fechafin);
            restart_fecha = fechaini;
            reend_fecha = fechafin;
            $("#dia_zoom").prop('checked', true);
            fechaini.setHours(fechaini.getHours());
            fechaini.setMinutes(fechaini.getMinutes());

            chart.zoomToDates(fechaini, fechafin);
        }
        function getSelectedRow() {
            var row = undefined;
            var sel = timeline.getSelection();
            if (sel.length) {
                if (sel[0].row != undefined) {
                    row = sel[0].row;
                }
            }
            return row;
        }
        function reloadcaseta(id) {
            document.getElementById("id_empresa").value = id_empresa;
            document.getElementById("equipo").value = id;
            document.myForm.submit();
        }
        var onEdit = function(event) {
            var row = getSelectedRow();
            var content = data.getValue(row, 2);
            var availability = strip(content);

            var newAvailability = prompt("Enter status\n\n" +
                "Choose from: Available, Unavailable, Maybe", availability);
            if (newAvailability != undefined) {
                var newContent = newAvailability;
                data.setValue(row, 2, newContent);
                data.setValue(row, 4, newAvailability.toLowerCase());
                timeline.draw(data);
            }
        };
        function addriego(resp) {
            var tipo2 = $('input:radio[name=tipo2]:checked').val()
            var tipo = $('input:radio[name=tipo]:checked').val()

            if ((tipo == 1) && (resp == 0)) {
                var sector = nombre;
                var fecha_fin = $('#datetimepicker_108').val();
                var fecha_ini = $('#datetimepicker_8').val()
                var largo = names.length;
                largo = largo + 1;
                names[largo] = sector;
                p_fecha_ini[largo] = fecha_ini;
                p_fecha_fin[largo] = fecha_fin;
                r_fecha_ini[largo] = fecha_ini;
                r_fecha_fin[largo] = fecha_ini;
                var name = sector;
                var start = new Date(fecha_ini);
                var end = new Date(fecha_fin);
                var availability = '<div onMouseOver=creainfo("' + largo + '"); onMouseOut=borrarinfo(); ><div onMouseOver=creainfo("' + n + '",event); style=" width: ' + 0 + '%; background-color: rgb(0, 119, 255); border-width:4px;" id="i' + largo + '";><div  style="color: #5D99C3;">&nbsp;</div></div></div>';
                var group = availability.toLowerCase();
                var content = availability;
                timeline.addItem({
                    'start': start,
                    'end': end,
                    'name': name,
                    'group': name,
                    'content': content

                });

                var count = data.getNumberOfRows();
                timeline.setSelection([{
                    'row': count - 1
                }]);
            } else if ((tipo == 2) && (resp == 0)) {
                var sector = nombre;
                var fecha_fin = $('#datetimepicker_108').val();
                var fecha_ini = $('#datetimepicker_8').val()
                var largo = names.length;
                largo = largo + 1;
                names[largo] = sector;
                p_fecha_ini[largo] = fecha_ini;
                p_fecha_fin[largo] = fecha_fin;
                r_fecha_ini[largo] = fecha_ini;
                r_fecha_fin[largo] = fecha_ini;
                var name = sector;
                var start = new Date(fecha_ini);
                var end = new Date(fecha_fin);
                var availability = '<div onMouseOver=creainfo("' + largo + '"); onMouseOut=borrarinfo(); ><div style=" width: ' + 0 + '%; background-color: rgb(0, 119, 255); border-width:4px;" id="i' + largo + '";><div  style="color: #5D99C3;">&nbsp;</div></div></div>';
                var group = availability.toLowerCase();
                var content = availability;
                timeline.addItem({
                    'start': start,
                    'end': end,
                    'name': name,
                    'group': name,
                    'content': content

                });

                var count = data.getNumberOfRows();
                timeline.setSelection([{
                    'row': count - 1
                }]);
            } else if ((tipo == 3) && (resp == 0)) {
                var sector = nombre;
                var fecha_fin = $('#datetimepicker_108').val();
                var fecha_ini = $('#datetimepicker_8').val()
                var largo = names.length;
                largo = largo + 1;
                names[largo] = sector;
                p_fecha_ini[largo] = fecha_ini;
                p_fecha_fin[largo] = fecha_fin;
                r_fecha_ini[largo] = fecha_ini;
                r_fecha_fin[largo] = fecha_ini;
                var name = sector;
                var start = new Date(fecha_ini);
                var end = new Date(fecha_fin);
                var availability = '<div onMouseOver=creainfo("' + largo + '"); onMouseOut=borrarinfo(); ><div style=" width: ' + 0 + '%; background-color: rgb(0, 119, 255); border-width:4px;" id="i' + largo + '";><div  onMouseOver=creainfo("' + largo + '",event); style="color: #5D99C3;">&nbsp;</div></div></div>';
                var group = availability.toLowerCase();
                var content = availability;
                timeline.addItem({
                    'start': start,
                    'end': end,
                    'name': name,
                    'group': name,
                    'content': content

                });

                var count = data.getNumberOfRows();
                timeline.setSelection([{
                    'row': count - 1
                }]);
            } else if ((tipo2 == 4) && (resp == 0)) {
                var sector = nombreFertil;
                var fechafinfertil = $('#fechafinfertil').val();
                var fechainifertil = $('#fechainifertil').val();
                var largo = names.length;
                largo = largo + 1;
                names[largo] = sector;
                p_fecha_ini[largo] = fechainifertil;
                p_fecha_fin[largo] = fechafinfertil;
                r_fecha_ini[largo] = fechainifertil;
                r_fecha_fin[largo] = fechainifertil;
                var name = sector;
                var start = new Date(fechainifertil);
                var end = new Date(fechafinfertil);
                var availability = '<div onMouseOver=creainfo("' + largo + '"); onMouseOut=borrarinfo(); ><div style=" width: ' + 0 + '%; background-color: rgb(0, 119, 255); border-width:4px;" id="i' + largo + '";><div  style="color: #5D99C3;">&nbsp;</div></div></div>';
                var group = availability.toLowerCase();
                var content = availability;
                timeline.addItem({
                    'start': start,
                    'end': end,
                    'name': name,
                    'group': name,
                    'content': content

                });

                var count = data.getNumberOfRows();
                timeline.setSelection([{
                    'row': count - 1
                }]);
            } else if ((tipo2 == 6) && (resp == 0)) {
                var sector = nombreFertil;
                var fechafinfertil = $('#fechafinfertil').val();
                var fechainifertil = $('#fechainifertil').val();
                var largo = names.length;
                largo = largo + 1;
                names[largo] = sector;
                p_fecha_ini[largo] = fechainifertil;
                p_fecha_fin[largo] = fechafinfertil;
                r_fecha_ini[largo] = fechainifertil;
                r_fecha_fin[largo] = fechainifertil;
                var name = sector;
                var start = new Date(fechainifertil);
                var end = new Date(fechafinfertil);
                var availability = '<div onMouseOver=creainfo("' + largo + '"); onMouseOut=borrarinfo(); ><div style=" width: ' + 0 + '%; background-color: rgb(0, 119, 255); border-width:4px;" id="i' + largo + '";><div  style="color: #5D99C3;">&nbsp;</div></div></div>';
                var group = availability.toLowerCase();
                var content = availability;
                timeline.addItem({
                    'start': start,
                    'end': end,
                    'name': name,
                    'group': name,
                    'content': content

                });

                var count = data.getNumberOfRows();
                timeline.setSelection([{
                    'row': count - 1
                }]);
            } else {
                sweetAlert("Oops...", "" + resp, "error");
            }
        }
        function mostrarpersostatos() {
            var myDiv = document.getElementById('divContenedor');
            var id_sectorTotal = <?php echo json_encode($id_sectorTotal); ?>;
            myDiv.scrollTop = 0;
            var color = document.getElementById("actualizar").style.backgroundColor;
            if (swPresostatos == 0) {
                var ancho_pantalla = $(window).width();
                var left_caudal = (div_ancho_nombre_sectores * 100) / ancho_pantalla;
                ancho_caudal = 100 - left_caudal;
                ancho_grafico_lineal = ancho_caudal - 0.575;
                ancho_caudal = ancho_grafico_lineal + "%";
                $("#chartdiv").css("width", ancho_caudal);
                document.getElementById("actualizar").style.backgroundColor = "#FDFDFD";
                document.getElementById("mytimeline").style.visibility = "hidden";
                document.getElementById("mytimeline2").style.visibility = "visible";
                swPresostatos = 1;
            } else {
                var ancho_pantalla = $(window).width();
                var left_caudal = (div_ancho_nombre_sectores * 100) / ancho_pantalla;
                ancho_caudal = 100 - left_caudal;
                ancho_grafico_lineal = ancho_caudal + 0.375;
                ancho_caudal = ancho_grafico_lineal + "%";
                $("#chartdiv").css("width", ancho_caudal);
                document.getElementById("divContenedor").style.overflow = "auto";
                document.getElementById("actualizar").style.backgroundColor = "white";
                document.getElementById("mytimeline").style.visibility = "visible";
                document.getElementById("mytimeline2").style.visibility = "hidden";
                if (id_sectorTotal.length < 11) {
                    document.getElementById("divContenedor").style.overflow = "hidden";
                }
                swPresostatos = 0;
            }


        }
        function validartiemporeal(id) {
            valor = document.getElementById(id).value;

            var res1 = id.split("_");
            id = res1[1];
            id = parseInt(id);
            if (id > 100) {
                id = id - 100;
            }
            preid = id - 1;
            j = id;
            valor = document.getElementById("sectores_" + j).value;
            if (tipoRiego[valor] == 1) {
                document.getElementById('grado_' + j).style.visibility = "hidden";
            } else {
                document.getElementById('grado_' + j).style.visibility = "visible";
            }
            id2 = id + 100;
            sector = document.getElementById('sectores_' + id).value;
            tipo = document.getElementById('tipos_' + id).value;
            semaforo1 = "verde";
            if (id == 8) {
                prinfechaini = $("#datetimepicker_8").val();
            } else {
                prinfechaini = $("#datetimepicker_" + id).val();
            }
            semaforo = "verde";

            if (id == 8) {
                tipoPrin = document.getElementById("tipos_8").value;
                if (tipoPrin == 2) {
                    duracionvalue = document.getElementById("duracionriego_8").value;
                    if (duracionvalue == "00:00") {
                        semaforo = 'rojo';
                    }
                }
                if (tipoPrin == 3) {
                    duracionvalue = document.getElementById("volumeninput_8").value;
                    if (duracionvalue == "") {
                        semaforo = 'rojo';
                    }
                }
            } else {
                tipoPrin = document.getElementById("tipos_" + id).value;
                if (tipoPrin == 2) {
                    duracionvalue = document.getElementById("duracionriego_" + id).value;
                    if (duracionvalue == "00:00") {
                        semaforo = 'rojo';
                    }
                }
                if (tipoPrin == 3) {
                    duracionvalue = document.getElementById("volumeninput_" + id).value;
                    if (duracionvalue == "") {
                        semaforo = 'rojo';
                    }
                }
                tipoPrin = document.getElementById("tipos_" + id).value;
            }
            if (tipoPrin == 2) {
                tiempofinPrin = document.getElementById('duracionriego_' + id).value;
                prinfechafin = $("#datetimepicker_" + id).val();
                var prinfechafin = new Date(prinfechafin);
                var res = tiempofinPrin.split(":");
                hora = res[0];
                minutos = res[1];
                minutos = parseInt(minutos);
                hora = parseInt(hora);
                hora = hora * 60 * 60;
                minutos = minutos * 60;
                segundos = hora + minutos;
                prinfechafin.setSeconds(segundos);


                dia = prinfechafin.getDate();
                if (dia < 10) {
                    dia = "0" + dia;
                }
                mes = prinfechafin.getMonth() + 1;
                if (mes < 10) {
                    mes = "0" + mes;
                }
                año = prinfechafin.getFullYear();

                hora = prinfechafin.getHours();
                if (hora < 10) {
                    hora = "0" + hora;
                }
                minuto = prinfechafin.getMinutes();
                if (minuto < 10) {
                    minuto = "0" + minuto;
                }

                prinfechafin = año + "-" + mes + "-" + dia + " " + hora + ":" + minuto;
            } else if (tipoPrin == 1) {
                prinfechafin = $("#datetimepicker_" + id2).val();
            }
            tipoSec = document.getElementById('tipos_' + j).value;
            //if(tipoWhile == 1){
            while (j > 8) {
                j = j - 1
                tipoSec = document.getElementById('tipos_' + j).value;
                if (tipoSec == 2) {
                    tiempofin = document.getElementById('duracionriego_' + j).value;
                    fechafinSec = $("#datetimepicker_" + j).val();
                    var fechafinSec = new Date(fechafinSec);
                    var res = tiempofin.split(":");
                    hora = res[0];
                    minutos = res[1];
                    minutos = parseInt(minutos);
                    hora = parseInt(hora);
                    hora = hora * 60 * 60;
                    minutos = minutos * 60;
                    segundos = hora + minutos;
                    fechafinSec.setSeconds(segundos);

                    dia = fechafinSec.getDate();
                    if (dia < 10) {
                        dia = "0" + dia;
                    }
                    mes = fechafinSec.getMonth() + 1;
                    if (mes < 10) {
                        mes = "0" + mes;
                    }
                    año = fechafinSec.getFullYear();
                    hora = fechafinSec.getHours();
                    if (hora < 10) {
                        hora = "0" + hora;
                    }
                    minuto = fechafinSec.getMinutes();
                    if (minuto < 10) {
                        minuto = "0" + minuto;
                    }
                    fechafinSec = año + "-" + mes + "-" + dia + " " + hora + ":" + minuto;
                } else if (tipoSec == 1) {
                    i = j + 100;
                    fechafinSec = $("#datetimepicker_" + i).val();
                }
                prinfechaini = $("#datetimepicker_" + id).val();
                fechainiSec = $("#datetimepicker_" + j).val();
                sector_prin = document.getElementById("sectores_" + id).value;
                sector_sec = document.getElementById("sectores_" + j).value;
                if (id_sector[sector_prin] == id_sector[sector_sec]) {
                    if ((fechainiSec < prinfechaini && fechafinSec > prinfechaini) ||
                        (fechainiSec < prinfechafin && fechafinSec > prinfechafin) ||
                        (fechainiSec < prinfechaini && fechafinSec > prinfechafin)) {
                        semaforo = 'rojo';
                        idfu = id;
                        document.getElementById("a_" + idfu).style.display = 'none';
                    }



                }
            }



            var data = 'id_sector=' + id_sector[sector] + '&fechaini=' + prinfechaini + '&fechafin=' + prinfechafin + '&tipo=' + tipo;
            $.ajax({
                url: 'ajax/validacionriegos.php',
                type: 'post',
                data: data,
                beforeSend: function() {},
                success: function(resp) {
                    if (resp == 0) {} else {
                        semaforo = 'rojo';
                        idfu = id + 1;
                        if (res1[0] == "a") {
                            document.getElementById("a_" + idfu).style.display = 'none';
                        }
                        document.getElementById("aceptar").disabled = 'disabled';
                    }
                    if (semaforo == 'verde') {
                        document.getElementById("g_" + id).style.backgroundColor = '#d2f5d2';
                    } else {
                        document.getElementById("a_" + id).style.display = 'none';
                        document.getElementById("g_" + id).style.backgroundColor = 'rgb(232, 183, 183)';
                    }
                }


            });
            if (semaforo == 'verde') {
                document.getElementById("a_" + id).style.display = "block";
                document.getElementById("aceptar").disabled = '';
            } else {
                document.getElementById("a_" + id).style.display = 'none';
                document.getElementById("aceptar").disabled = 'disable';
                id2 = id + 1;
                document.getElementById("a_" + id2).style.display = 'none';
            }
        }
        function eliminarnuevoriego(id) {
            cantidadderiegos = cantidadderiegos - 1;
            var res = id.split("_");
            newid = res[1];
            newid = parseInt(newid);
            idpre = newid - 1;
            idprecien = idpre + 100;
            newid2 = newid;
            newid2 = newid2 - 1;
            newidpre = newid - 1;
            newid3 = "e_" + newid2;
            newid2 = "a_" + newid2;
            document.getElementById("" + newid2 + "").style.display = "block";
            document.getElementById("" + newid3 + "").style.display = "block";
            //document.getElementById("g_"+newidpre).style.backgroundColor = 'white';
            document.getElementById("aceptar").style.float = "right";
            document.getElementById('sectores_' + idpre).disabled = "";
            document.getElementById('datetimepicker_' + idpre).disabled = "";
            document.getElementById('tipos_' + idpre).disabled = "";
            document.getElementById('duracionriego_' + idpre).disabled = "";
            document.getElementById('datetimepicker_' + idprecien).disabled = "";
            document.getElementById('volumeninput_' + idpre).disabled = "";
            document.getElementById('grado_' + idpre).disabled = "";
            $("#g_" + newid + "").remove();
        }
        function agregarnuevoriego(id) {
            cantidadderiegos = cantidadderiegos + 1;
            document.getElementById("" + id + "").style.display = "none";
            var res = id.split("_");
            newid = res[1];
            newid = parseInt(newid);
            j = newid;
            eid = newid;
            eid = "e_" + eid;
            aid = newid;
            document.getElementById("" + eid + "").style.display = "none";
            newid = parseInt(newid);
            newid = newid + 1;
            newid2 = newid + 100;
            newpre2 = newid - 1;
            newpre = newid2 - 1;

            document.getElementById("a_" + aid).style.display = 'none';
            document.getElementById('sectores_' + aid).disabled = "disabled";
            document.getElementById('datetimepicker_' + aid).disabled = "disabled";
            document.getElementById('tipos_' + aid).disabled = "disabled";
            document.getElementById('duracionriego_' + aid).disabled = "disabled";
            document.getElementById('datetimepicker_' + newpre).disabled = "disabled";
            document.getElementById('volumeninput_' + aid).disabled = "disabled";
            document.getElementById("aceptar").disabled = 'disabled';
            document.getElementById("grado_" + aid).disabled = 'disabled';
            if (tipoRiego[0] == 2) {
                visible = "visible";
            } else {
                visible = "hidden";
            }
            $("#padremodal").append("<div class='col-md-12' id='g_" + newid + "'>" +
                "<div class='col-lg-2'><select class='form-control' id='sectores_" + newid + "' onchange='validartiemporeal(id);'>" +
                "</select></div>" +
                "<div class='col-lg-3' id='fechaini_" + newid2 + " style='width: 191px;'>" +
                "<div class='input-group date'>" +
                "<input type='text' id='datetimepicker_" + newid + "' onChange='fechamin1(id); validartiemporeal(id);'  class='form-control' />" +
                "</div>" +
                "</div>" +
                "<div class='col-md-2'>" +
                "<select class='form-control' onChange='selecciontipo(id); validartiemporeal(id);' id='tipos_" + newid + "''>" +
                "<option  value='2'>Duracion</option>" +
                //"<option  value='1'>Fecha inicio</option>"+
                "<option  value='3'>M3</option>" +
                "</select>" +
                "</div>" +
                "<div class='col-md-3' id='fechafin_" + newid2 + "'' style='display:none'>" +
                "<div class='input-group date' >" +
                "<input type='text' id='datetimepicker_" + newid2 + "' class='form-control' onChange='validartiemporeal(id);' />" +
                "</div>" +
                "</div>" +
                "<div class='col-md-2' style='display:block;'' id='duracion_" + newid2 + "''>" +
                "<input id='duracionriego_" + newid + "'' value='00:00' type='time' class='form-control' onChange='validartiemporeal(id);'/>" +
                "</div>" +
                "<div class='col-md-1' style='visibility:" + visible + ";'' id='duracion_" + newid2 + "''>" +
                "<input id='grado_" + newid + "'' value='0' type='input' class='form-control' onChange='validartiemporeal(id);'/>" +
                "</div>" +
                "<div class='col-md-2' style='display:none;'' id='volumen_" + newid2 + "''>" +
                "<input id='volumeninput_" + newid + "'' placeholder='m3'type='text' class='form-control' onChange='validartiemporeal(id);'/>" +
                "</div>" +
                "<div class='col-md-1'><a id='a_" + newid + "' onclick='validartiemporeal(id); agregarnuevoriego(id); ' style='cursor:pointer;'><img class='hidden-xs' src='imagenes/agregar.png'" +
                "style='width:28px;'></a>" +
                "</div>" +
                "<div class='col-md-1'><a id='e_" + newid + "'' onclick='eliminarnuevoriego(id);' style='cursor:pointer;'><img class='hidden-xs' src='imagenes/trash.png'" +
                "style='width:28px;'></a>" +
                "</div>");
            var largo = nombre_sector_control.length;
            for (i = 0; i < largo; i++) {
                $("#sectores_" + newid).append(' <option value="' + i + '">' + nombre_sector_control[i] + '</option>');
            }
            $("#datetimepicker_" + newid).kendoDateTimePicker({
                value: new Date(),
                interval: 1,
                format: 'yyyy-MM-dd HH:mm',
                timeFormat: "HH:mm",
                culture: "es-ES",
                min: new Date()
            });
            $("#datetimepicker_" + newid2).kendoDateTimePicker({
                value: new Date(),
                interval: 1,
                format: 'yyyy-MM-dd HH:mm',
                timeFormat: "HH:mm",
                culture: "es-ES",
                min: new Date()
            });

            var id4 = newid2 - 1;
            var newid4 = newid - 1;
            tipoSec = document.getElementById('tipos_' + newid4).value;
            if (tipoPrin == 2) {
                tiempofinPrin = document.getElementById('duracionriego_' + newid4).value;
                prinfechafin = $("#datetimepicker_" + newid4).val();
                var prinfechafin = new Date(prinfechafin);
                var res = tiempofinPrin.split(":");
                hora = res[0];
                minutos = res[1];
                minutos = parseInt(minutos);
                hora = parseInt(hora);
                hora = hora * 60 * 60;
                minutos = minutos * 60;
                segundos = hora + minutos + 60;
                prinfechafin.setSeconds(segundos);


                dia = prinfechafin.getDate();
                if (dia < 10) {
                    dia = "0" + dia;
                }
                mes = prinfechafin.getMonth() + 1;
                if (mes < 10) {
                    mes = "0" + mes;
                }
                año = prinfechafin.getFullYear();

                hora = prinfechafin.getHours();
                if (hora < 10) {
                    hora = "0" + hora;
                }
                minuto = prinfechafin.getMinutes();
                if (minuto < 10) {
                    minuto = "0" + minuto;
                }

                prinfechafin = año + "-" + mes + "-" + dia + " " + hora + ":" + minuto;
                $("#datetimepicker_" + newid).val(prinfechafin);
                $("#datetimepicker_" + newid2).val(prinfechafin);
            } else if (tipoPrin == 1) {
                $("#datetimepicker_" + newid).val($("#datetimepicker_" + id4).val());
                $("#datetimepicker_" + newid2).val($("#datetimepicker_" + id4).val());
                //prinfechafin = $("#datetimepicker_"+id2).val();
            }


        }
        function selecciontipo(id) {
            var res = id.split("_");
            newid = res[1];
            newid = parseInt(newid);
            newid2 = newid;
            if (newid != 8) {
                newid = newid + 100;
            }
            a = $("#tipos_" + newid2 + "").val();
            if (a == 1) {
                document.getElementById("fechafin_" + newid + "").style.display = "block";
                document.getElementById("duracion_" + newid + "").style.display = "none";
                document.getElementById("volumen_" + newid + "").style.display = "none";
            }
            if (a == 2) {
                document.getElementById("fechafin_" + newid + "").style.display = "none";
                document.getElementById("duracion_" + newid + "").style.display = "block";
                document.getElementById("volumen_" + newid + "").style.display = "none";
            }
            if (a == 3) {
                document.getElementById("fechafin_" + newid + "").style.display = "none";
                document.getElementById("duracion_" + newid + "").style.display = "none";
                document.getElementById("volumen_" + newid + "").style.display = "block";
            }
        }
        function eliminarRiego(id) {
            document.getElementById("id_riego").value = id;
        }
        function entra1() {
            document.getElementById("fechafin").style.display = "block";
            document.getElementById("volumen").style.display = "none";
            document.getElementById("duracion").style.display = "none";
        }
        function entra2() {
            document.getElementById("fechafin").style.display = "none";
            document.getElementById("volumen").style.display = "block";
            document.getElementById("duracion").style.display = "none";
        }
        function entra3() {

            document.getElementById('duracionFerti').checked = false;
            document.getElementById("duracion2").style.display = "none";
            document.getElementById("fechafin").style.display = "none";
            document.getElementById("volumen").style.display = "none";
            document.getElementById("duracion").style.display = "block";
        }
        function entraIni() {
            if (tipoRiego[0] == 2) {
                document.getElementById('gradolabbel').style.visibility = "visible";
                document.getElementById('grado_8').style.visibility = "visible";
            }

            document.getElementById('duracionFerti').checked = false;
            document.getElementById("duracion2").style.display = "none";
        }
        function entra4() {
            document.getElementById("fechafin2").style.display = "block";
            document.getElementById("volumen2").style.display = "none";
            document.getElementById("duracion2").style.display = "none";
        }
        function entra5() {
            document.getElementById("fechafin2").style.display = "none";
            document.getElementById("volumen2").style.display = "block";
            document.getElementById("duracion2").style.display = "none";
        }
        function entra6() {

            document.getElementById("fechafin2").style.display = "none";
            document.getElementById("volumen2").style.display = "none";
            document.getElementById("duracion2").style.display = "block";
        }
        function notificiaciones() {
            $numero_li = $("#lista2 li").size();
            if ($numero_li == 0) {
                swal("", "No tiene alertas activas.", "success");
            } else {
                var estadolista = document.getElementById("lista2").style.display;
                if (estadolista == "block") {
                    document.getElementById("lista2").style.display = "none";
                    document.getElementById("arrow_up").style.display = "none";
                } else {
                    document.getElementById("lista2").style.display = "block";
                    document.getElementById("arrow_up").style.display = "block";
                }
            }

        }
        function borrarinfo() {

            $(".info").remove();
            document.getElementById("mytimeline").style.zIndex = '0';
        }
        function activacionManualNodos() {
            var data = 'equipo_post=' + equipo_post + '&id_empresa=' + id_empresa;
            $.ajax({
                url: 'ajax/sectoresEquipo.php',
                type: 'post',
                data: data,
                beforeSend: function() {},
                success: function(resp) {
                    sectores = JSON.parse(resp);
                    largo = sectores.length;
                    $("#campos").empty();
                    $("#campos").append(' <option value="">Sectores</option>');
                    for (i = 0; i < largo; i++) {
                        $("#campos").append(' <option value="' + sectores[i].id_sector + '">' + sectores[i].nombre_sector + '</option>');
                    }
                }
            })
        }
        function actualizarOnOf() {
            var color = document.getElementById("actualizar").style.backgroundColor;
            if (color == "white") {
                document.getElementById("actualizar").style.backgroundColor = "skyblue";

            } else {
                clearInterval(repeticion1);
                clearInterval(repeticion2);
                document.getElementById("actualizar").style.backgroundColor = "white";
            }

        }
        function modificar(id_sector) {
            var data = 'empresa=' + id_empresa + '&equipo=' + equipo_post + '&id_sector=' + id_sector;
            $.ajax({
                url: 'ajax/estadoNuevoNodo.php',
                type: 'post',
                data: data,
                beforeSend: function() {},
                success: function(resp) {

                    componentes_sector = JSON.parse(resp);
                    largo_componente = componentes_sector.length;
                    var x = 0;
                    $('#estado_nodo').remove();
                    $('#listaSectores').append('<table id="estado_nodo" class="table" order="1"><thead><tr><th>Componente</th><th>Id nodo</th><th>Estado</th><th></th></tr></thead><tbody id="cuerpo_tabla_componentes"></tbody></table>');
                    while (x < largo_componente) {
                        switch (componentes_sector[x]['estado']) {
                            case '0':
                                var estado = 'Detenido';
                                var image = new Image();
                                var src = 'imagenes/stop.png';
                                image.src = src;
                                image.width = '25';
                                image.width = '25';
                                break;
                            case '1':
                                var estado = 'Funcionando';
                                var image = new Image();
                                var src = 'imagenes/start.png';
                                image.src = src;
                                image.width = '25';
                                image.width = '25';
                                break;
                            case '2':
                                var estado = 'Detenido';
                                var image = new Image();
                                var src = 'imagenes/stop.png';
                                image.src = src;
                                image.width = '25';
                                image.width = '25';
                                break;
                            case '3':
                                var estado = 'Error';
                                var image = new Image();
                                var src = 'imagenes/error.png';
                                image.src = src;
                                image.width = '25';
                                image.width = '25';
                                break;
                            case '5':
                                var estado = 'Reiniciando';
                                var image = new Image();
                                var src = 'imagenes/reset.png';
                                image.src = src;
                                image.width = '25';
                                image.width = '25';
                                break;
                        }
                        $("#cuerpo_tabla_componentes").append('<tr><td>' + componentes_sector[x]['nombre_sector'] + '</td><td>' + componentes_sector[x]['id_nodo'] + '</td><td>' + estado + '<div id="imagen_' + x + '"style="float: left;margin-right: 5px;"></div></td><td><button type="button" onclick="actualizarInfoComponentes(' + id_sector + ',' + componentes_sector[x]['id_nodo'] + ');" class="btn btn-warning">reiniciar</button></td></tr>');
                        $('#imagen_' + x).append(image);
                        x = x + 1;
                    }
                }
            })
        }
        function actualizarInfoComponentes(id_sector, id_nodo) {
            var fecha_actual = moment().format("YYYY-MM-DD HH:mm");
            fecha_fin = moment().add(4, 'minutes');
            var fecha_fin = moment(fecha_fin).format("YYYY-MM-DD HH:mm");
            fechainiJSON = JSON.stringify(fecha_actual);
            fechafinJSON = JSON.stringify(fecha_fin);
            tipo = 1;
            var id_empresa = <?php echo $id_empresa; ?>;
            var data = 'fechafinJSON=' + fechafinJSON + '&id_empresa=' + id_empresa + '&id_nodo=' + id_nodo + '&fechainiJSON=' + fechainiJSON + '&sector=' + id_sector + '&iteracion=' + -1 + '&tipo5=' + tipo + '&grados=' + 0;
            $.ajax({
                url: 'ajax/insertar_reset_json.php',
                type: 'post',
                data: data,
                async: false,
                beforeSend: function() {
                    $('.bs-example-modal-sm').modal('show');
                },
                success: function(resp) {
                    console.log(resp);
                    $('.bs-example-modal-sm').modal('hide');
                    if (resp == 1) {
                        swal("Componente Reiniciandoce", "", "success");
                    } else {
                        swal("Ya se esta intentando reiniciar este componente", "", "error");
                    }
                }
            });
            modificar(id_sector);
            if (actualizarNodos != null) {
                clearInterval(actualizarNodos);
            }
            actualizarNodos = window.setInterval("modificar(" + id_sector + ")", 120000); //Cambiar a 120000
        }
        function creainfo(indice, event) {
            if ($("#info").length > 0) {
                $(".info").remove();
                document.getElementById("mytimeline").style.zIndex = '0';
                return 0;
            }
            var data = 'equipo_post=' + equipo_post + '&id_empresa=' + id_empresa;
            $.ajax({
                url: 'ajax/actualizarinfo.php',
                type: 'post',
                data: data,
                beforeSend: function() {},
                success: function(resp) {
                    $(".info").remove();
                    riegos = JSON.parse(resp);
                    var desde = riegos[indice].p_fecha_ini;
                    var hasta = riegos[indice].p_fecha_fin;
                    var desdeP = new Date(riegos[indice].p_fecha_ini);
                    var hastaP = new Date(riegos[indice].p_fecha_fin);
                    var duracion = hastaP - desdeP;
                    duracion = ((duracion / 1000) / 60);
                    duracion = Math.round(duracion);
                    var ini = desde.split(" ");
                    var fin = hasta.split(" ");
                    var start2 = new Date(riegos[indice].r_fecha_ini);
                    var end2 = new Date(riegos[indice].r_fecha_fin);
                    var duracion2 = end2 - start2;
                    duracion2 = ((duracion2 / 1000) / 60);
                    duracion2 = duracion2 + riegos[indice].minT;
                    var duracionReal = Math.round(duracion2)
                    if ($("#info").length > 0) {
                        $(".info").remove();
                        document.getElementById("mytimeline").style.zIndex = '0';
                        return 0;
                    }
                    $("#infoline").append('<div id="info" class="info" style="text-align: left;" onmouseout="borrarinfo();"><strong>Informacion</strong></br><div style="width: 100%;height: 3px;background-color: #406a87;"></div><strong >Inicio Agendado del Riego:</strong>' + ini[1] + '</br><Strong>Fin Agendado del Riego:</strong>' + fin[1] + '</br><strong >Tiempo Total de Duracion:</strong>' + duracion + '&nbsp; min</br><strong >Tiempo Real de Riego:</strong>' + duracionReal + '&nbsp;min</div>');

                    var tempX = event.pageX + "px";
                    var tempY = event.pageY + "px";

                    $("#info").css("position", "absolute");
                    $("#info").css("left", tempX);
                    $("#info").css("top", tempY);
                    $("#info").css("background-color", "white");
                    $("#info").css("border-style", "solid");
                    $("#info").css("border-color", "#4d4d4d");
                    $("#info").css("border-width", "2px");
                    $("#info").css("padding", "8px");
                    $("#info").css("z-index", "9");
                    $("#info").css("border-radius", "7px");
                }
            })
        }
        function setMes() {
            var i = new Date();
            var d = new Date();
            d.setDate(d.getDate() - 30);
            timeline.setVisibleChartRange(d, i);
            var div = document.getElementById("select-choice-1");
            div.style.display = "none";
            $("#colophon").css("height", "3.4%");
        }
        function setSemana() {
            var i = new Date();
            var d = new Date();
            d.setDate(d.getDate() - 7);
            timeline.setVisibleChartRange(d, i);
            var div = document.getElementById("select-choice-1");
            div.style.display = "none";
            $("#colophon").css("height", "3.4%");
        }
        function setDia() {
            var i = new Date();
            var d = new Date();
            d.setDate(d.getDate() - 1);
            timeline.setVisibleChartRange(d, i);
            var div = document.getElementById("select-choice-1");
            div.style.display = "none";
            $("#colophon").css("height", "3.4%");
        }
        function doDelete() {
            var sel = timeline.getSelection();
            if (sel.length) {
                if (sel[0].row != undefined) {
                    var row = sel[0].row;
                }
            }
            var idr = id[row];
            clearInterval(repeticion2);
            swal({
                    title: "Escriba su clave de Administrador",
                    type: "input",
                    confirmButtonText: "Aceptar",
                    cancelButtonText: "Cancelar",
                    confirmButtonColor: "#37E52E",
                    showCancelButton: true,
                    closeOnConfirm: false,
                    animation: "slide-from-top"
                },
                function(inputValue) {
                    if (inputValue === false) {
                        repeticion2 = window.setInterval("actualizarRiegos()", 120000);
                        return false;
                    } else {
                        repeticion2 = window.setInterval("actualizarRiegos()", 120000);
                        if (inputValue === "") {
                            swal.showInputError("El campo no puede quedar vacio!");
                            return false
                        }
                        //swal("Nice!", "You wrote: " + inputValue, "success");
                        var clave_aux = document.getElementById('pass_aux').value;
                        $.ajax({
                            url: 'ajax/pass_control.php',
                            type: 'post',
                            data: 'clave=' + clave_aux + '&id_empresa=' + id_empresa,
                            beforeSend: function() {

                            },
                            success: function(resp) {
                                if (resp == 1) {

                                    var now = new Date();
                                    var inir = new Date(p_fecha_ini[row]);
                                    var finr = new Date(p_fecha_fin[row]);
                                    if (now > inir && now < finr) {
                                        swal({
                                                title: "¿Estas seguro?",
                                                text: "El riego que ha seleccionado sera re-agendado para que finalice en este momento!",
                                                type: "warning",
                                                showCancelButton: true,
                                                confirmButtonColor: "#DD6B55",
                                                confirmButtonText: "Si",
                                                cancelButtonText: "No",
                                                closeOnConfirm: false,
                                                closeOnCancel: false
                                            },
                                            function(isConfirm) {
                                                if (isConfirm) {
                                                    var data = 'id=' + idr + '&id_empresa=' + id_empresa;
                                                    $.ajax({
                                                        url: 'ajax/updateRiego.php',
                                                        type: 'post',
                                                        data: data,
                                                        beforeSend: function() {},
                                                        success: function(resp) {

                                                            console.log(resp);
                                                            if (resp == 404) {
                                                                swal("Error!", "Revise su conexión a Internet.", "error");
                                                                repeticion2 = window.setInterval("actualizarRiegos()", 120000);
                                                            } else {
                                                                swal("Eliminado!", "Su riego ha sido cancelado.", "success");
                                                                javascript: location.reload();
                                                            }
                                                        }
                                                    })

                                                } else {
                                                    swal("Cancelado!", "Su riego NO ha sido re-agenado para finalizar en este momento.", "error");
                                                    repeticion2 = window.setInterval("actualizarRiegos()", 120000);
                                                }
                                            });
                                    } else if (now > inir && now > finr) {
                                        sweetAlert("Oops...", "No puede cancelar un riego que ya se realizo", "error");
                                    } else {
                                        swal({
                                                title: "¿Estas seguro?",
                                                text: "El riego que ha seleccionado sera re-agendado para que finalice en este momento!",
                                                type: "warning",
                                                showCancelButton: true,
                                                confirmButtonColor: "#DD6B55",
                                                confirmButtonText: "Si",
                                                cancelButtonText: "No",
                                                closeOnConfirm: false,
                                                closeOnCancel: false
                                            },
                                            function(isConfirm) {
                                                if (isConfirm) {
                                                    var data = 'id=' + idr + '&id_empresa=' + id_empresa;
                                                    $.ajax({
                                                        url: 'ajax/updateRiego.php',
                                                        type: 'post',
                                                        data: data,
                                                        beforeSend: function() {},
                                                        success: function(resp) {
                                                            //javascript:location.reload();
                                                            if (resp == 404) {
                                                                swal("Error!", "Revise su conexión a Internet.", "error");
                                                                repeticion2 = window.setInterval("actualizarRiegos()", 120000);
                                                            } else {
                                                                swal("Eliminado!", "Su riego ha sido cancelado.", "success");
                                                                javascript: location.reload();
                                                            }
                                                        }
                                                    })

                                                } else {
                                                    swal("Cancelado!", "Su riego NO ha sido re-agenado para finalizar en este momento.", "error");
                                                    repeticion2 = window.setInterval("actualizarRiegos()", 120000);
                                                }
                                            });
                                    }

                                    if (row != undefined) {
                                        // timeline.deleteItem(row);
                                    } else {
                                        sweetAlert("Error", "Debe seleccionar un Riego", "error");
                                        repeticion2 = window.setInterval("actualizarRiegos()", 120000);
                                    }
                                } else if (resp == 2) {
                                    swal.showInputError("Clave invalida, intente de nuevo!");
                                    return false
                                }
                            },
                            error: function(resp) {
                                alert("Unexpected error! Try again." + resp);
                            }
                        })
                    }
                }


            )

        }
        function doReset() {
            var sel = timeline.getSelection();
            if (sel.length) {
                if (sel[0].row != undefined) {
                    var row = sel[0].row;
                }
            }
            var idr = id[row];
            var now = new Date();
            var inir = new Date(p_fecha_ini[row]);
            var finr = new Date(p_fecha_fin[row]);
            if (now > inir && now < finr) {

                swal({
                        title: "¿Estas seguro?",
                        text: "Reiniciara El cordinador",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Si",
                        cancelButtonText: "No",
                        closeOnConfirm: false,
                        closeOnCancel: false
                    },
                    function(isConfirm) {
                        if (isConfirm) {
                            document.getElementById('i' + row).style.backgroundColor = 'rgb(195, 49, 45)';
                            var data = 'id=' + idr;
                            $.ajax({
                                url: 'ajax/updateRiego2.php',
                                type: 'post',
                                data: data,
                                beforeSend: function() {},
                                success: function(resp) {
                                    javascript: location.reload();
                                }

                            })


                            swal("Aceptado!", "Su cordinador se a reiniciado.", "success");
                        } else {
                            swal("Cancelado!", "Su cordinador no se a reiniciado.", "error");
                        }
                    });
            } else if (now > inir && now > finr) {
                sweetAlert("Oops...", "No puede cancelar un riego que ya se realizo", "error");
            } else {
                swal({
                        title: "¿Estas seguro?",
                        text: "El riego que ha seleccionado sera cancelado!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Si",
                        cancelButtonText: "No",
                        closeOnConfirm: false,
                        closeOnCancel: false
                    },
                    function(isConfirm) {
                        if (isConfirm) {
                            swal("Eliminado!", "Su riego ha sido cancelado.", "success");
                            var data = 'id=' + idr;
                            $.ajax({
                                url: 'ajax/deleteRiego.php',
                                type: 'post',
                                data: data,
                                beforeSend: function() {

                                },
                                success: function(resp) {
                                    timeline.deleteItem(row);
                                    javascript: location.reload();
                                }
                            })
                        } else {
                            swal("Cancelado!", "Su riego no ha sido eliminado.", "error");
                        }
                    });
            }

            if (row != undefined) {
                // timeline.deleteItem(row);
            } else {
                sweetAlert("Error", "Debe seleccionar un Riego", "error");
            }
        }
        function consultandoEstado(id, h) {
            var data = 'id=' + id;
            $.ajax({
                url: 'ajax/nodoEstado.php',
                type: 'post',
                data: data,
                beforeSend: function() {},
                success: function(resp) {
                    if (resp == 2) {
                        document.getElementById('detenido' + h + '').checked = true;
                        document.getElementById("cargando" + h + "").style.visibility = "hidden";
                        document.getElementById("reset" + h + "").style.visibility = "visible";
                        document.getElementById("reseteando" + h + "").style.visibility = "hidden";
                        swal("Bien!", "Su nodo se ha reiniciado.", "success");
                        swReset = 0;
                        estadonodoTotal[0][h] = 2;
                        window.clearInterval(repeticion);
                    } else if (resp == 3) {

                        $.ajax({
                            url: 'ajax/errorNodo.php',
                            type: 'post',
                            data: data,
                            beforeSend: function() {},
                            success: function(resp) {
                                document.getElementById("reset" + h + "").style.visibility = "visible";
                                document.getElementById("reseteando" + h + "").style.visibility = "hidden";
                                document.getElementById("cargando" + h + "").style.visibility = "hidden";
                                document.getElementById('error' + h + '').checked = true;
                                swal("Lo sentimos", "Su nodo no responde.", "error");
                                window.clearInterval(repeticion);
                                swReset = 0;
                            }
                        })
                    }

                }
            })
        }
        function resetNodo(id, h, px) {
            if (swReset == 0) {
                swReset = 1
            } else {

                swal("Lo sentimos", "Solo puede reiniciar un elemento a la vez.", "error");
                return 0;
            }
            var data = 'id_nodo=' + id + '&px=' + px;
            document.getElementById("reset" + h + "").style.visibility = "hidden";
            document.getElementById("reseteando" + h + "").style.visibility = "visible";
            document.getElementById("cargando" + h + "").style.visibility = "visible";
            document.getElementById("cargando" + h + "").style.position = "relative";
            document.getElementById("cargando" + h + "").style.top = "-2px";
            document.getElementById("cargando" + h + "").style.height = "32px";

            $.ajax({
                url: 'ajax/resetNodo.php',
                type: 'post',
                data: data,
                beforeSend: function() {},
                success: function(resp) {
                    repeticion = window.setInterval("consultandoEstado(" + id + "," + h + ")", 60000);
                }
            })
        }
        function moverDOM() {

            var ancho_label = $('.timeline-groups-axis').width();
            var ancho_pantalla = screen.width;
            var largo_porcentaje = ancho_label * 100;
            largo_porcentaje = largo_porcentaje / ancho_pantalla;

            var ancho_axesy = $('.graph-axis-vertical').width();
            var largo_axesy = ancho_axesy * 100;
            largo_axesy = largo_axesy / ancho_pantalla;

            var left_axesy = 8;
            var left_axesy = left_axesy * 100;
            left_axesy = left_axesy / ancho_pantalla;

            largo_porcentaje = largo_porcentaje - largo_axesy - left_axesy - 0.1;
            var ancho_timeline = $('.timeline-content').width();
            var ancho_timeline = ancho_timeline * 100;
            ancho_timeline = ancho_timeline / ancho_pantalla;
            if ($('#legend_lineal').html()) {
                $('#legend_lineal').empty();
                $('.graph-legend').appendTo('#legend_lineal');
                $('.graph-legend').css({
                    "overflow-x": "auto",
                    "text-align": "-webkit-auto",
                    "position": "absolute",
                    "overflow-y": "auto",
                    "max-width": "13.5%",
                    "z-index": "5"
                });
            } else {
                $('.graph-legend').appendTo('#legend_lineal');
                $('.graph-legend').css({
                    "overflow-x": "auto",
                    "text-align": "-webkit-auto",
                    "position": "absolute",
                    "overflow-y": "auto",
                    "max-width": "13.5%",
                    "z-index": "5"
                });

            }
            $('.graph-frame').css({
                "left": "11.28799%"
            });

        }
        function configSectores() {
            var sectores = <?php echo json_encode($id_sector2); ?>;
            var indice = document.getElementById("campos").value;
            var sectores2 = sectores[indice];
            $.ajax({
                url: 'ajax/criterioSector.php',
                type: 'post',
                data: 'id_sector=' + sectores2,
                beforeSend: function() {},
                success: function(resp) {
                    var nodos = JSON.parse(resp);
                    document.getElementById("caudalInferior").value = nodos[0].caudalInferior;
                    document.getElementById("caudalSuperior").value = nodos[0].caudalSuperior;

                    document.getElementById("tiempoInferior").value = nodos[0].tiempoInferior;
                    document.getElementById("tiempoSuperior").value = nodos[0].tiempoSuperior;

                    document.getElementById("prescionInferior").value = nodos[0].prescionInferior;
                    document.getElementById("prescionSuperior").value = nodos[0].prescionSuperior;
                },
                error: function(resp) {
                    alert("Unexpected error! Try again." + resp);
                }
            })
        }
        function abre_fecha_fin() {
            fecha_fin_aux.enable();
            var fecha_aux = $('#fecha_inicio').val();
            fecha_fin_aux.value($('#fecha_inicio').val());
            fecha_aux = new Date(fecha_aux);

            var day = fecha_aux.getDate() + 2;
            var month = fecha_aux.getMonth();
            var year = fecha_aux.getFullYear();
            //alert(year+"--"+month+"--"+day);
            fecha_fin_aux.min(new Date(year, month, day));
        }
        function recallfechaini() {
            $("#datetimepicker_8").kendoDateTimePicker({
                value: new Date(),
                interval: 1,
                format: 'yyyy-MM-dd HH:mm',
                timeFormat: "HH:mm",
                culture: "es-ES",
                min: new Date()
            });
            var datepicker2 = $("#datetimepicker_8").data("kendoDateTimePicker");


            $("#datetimepicker_108").kendoDateTimePicker({
                value: new Date(),
                interval: 1,
                format: 'yyyy-MM-dd HH:mm',
                timeFormat: "HH:mm",
                culture: "es-ES",
                min: new Date()
            });
            var datepicker3 = $("#datetimepicker_108").data("kendoDateTimePicker");
        }
        function setNow() {

            var fecha_aux_ini = $('#fecha_inicio').val();
            var fecha_aux_fin = $('#fecha_fin').val();
            if (fecha_aux_ini == "" || fecha_aux_fin == "") {

            } else {
                var i = new Date(fecha_aux_fin);
                var d = new Date(fecha_aux_ini);
                //d.setDate(d.getDate() - 1);
                timeline.setVisibleChartRange(d, i);
                var div = document.getElementById("select-choice-1");
                div.style.display = "none";
                $("#colophon").css("height", "3.4%");
            }
        }
        function detectar_pausa_play() {
            $.ajax({
                url: 'ajax/detectar_pausa_play.php',
                type: 'post',
                data: 'id_empresa=' + id_empresa + '&id_equipo=' + equipo_post,
                beforeSend: function() {

                },
                success: function(resp) {
                    if (resp == 0) {
                        $('#play').hide();
                        $('#pausa').show();
                    } else {
                        $('#pausa').hide();
                        $('#play').show();
                    }
                }
            });
        }
        var datepicker7 = $("#datetimepicker7").data("kendoDateTimePicker");

        function fechamin1(id) {
            id = id.match(/\d+/g).join('');
            id = parseInt(id);
            newid = id + 100;
            //datepicker3.enable();
            //var fecha_aux = $('#datetimepicker_8').val();
            //datepicker3.value($('#datetimepicker_8').val());
            $("#datetimepicker_" + newid).val($("#datetimepicker_" + id).val());
            //datepicker2.min(new Date(year, month, day, hour, minutes));
        }
        function fechamin2() {
            datepicker5.enable();
            var fecha_aux = $('#datetimepicker4').val();
            datepicker5.value($('#datetimepicker4').val());
            fecha_aux = new Date(fecha_aux);
            var minutes = fecha_aux.getMinutes();
            var hour = fecha_aux.getHours();
            var day = fecha_aux.getDate();
            var month = fecha_aux.getMonth();
            var year = fecha_aux.getFullYear();
            datepicker5.min(new Date(year, month, day, hour, minutes));
        }
        function pasaporte(n) {

            clearInterval(repeticion2);
            swal({
                    title: "Escriba su clave de Administrador",
                    type: "input",
                    confirmButtonText: "Aceptar",
                    cancelButtonText: "Cancelar",
                    confirmButtonColor: "#37E52E",
                    showCancelButton: true,
                    closeOnConfirm: false,
                    animation: "slide-from-top"
                },
                function(inputValue) {
                    if (inputValue === false) {
                        repeticion2 = window.setInterval("actualizarRiegos()", 120000);
                        return false;
                    } else {
                        repeticion2 = window.setInterval("actualizarRiegos()", 120000);
                        if (inputValue === "") {
                            swal.showInputError("El campo no puede quedar vacio!");
                            return false
                        }
                        //swal("Nice!", "You wrote: " + inputValue, "success");
                        var clave_aux = document.getElementById('pass_aux').value;
                        $.ajax({
                            url: 'ajax/pass_control.php',
                            type: 'post',
                            data: 'clave=' + clave_aux + '&id_empresa=' + id_empresa,
                            beforeSend: function() {

                            },
                            success: function(resp) {
                                if (resp == 1) {
                                    funcion_a_llamar(n);
                                } else if (resp == 2) {
                                    swal.showInputError("Clave invalida, intente de nuevo!");
                                    return false
                                }
                            },
                            error: function(resp) {
                                alert("Unexpected error! Try again." + resp);
                            }
                        })
                    }
                }


            )
        }
        function funcion_a_llamar(n) {
            if (n == 1) {
                swal.close();
                insertar_riego();
            } else if (n == 2) {
                update_control();
            } else if (n == 3) {
                doDelete();
            } else if (n == 4) {
                doReset();
            }
        }
        function compuesto(a) {
            tipoderiego = a;
        }
        function insertar_riego() {
            p = 0;
            var fechainiJSON = new Array();
            var fechafinJSON = new Array();
            var sector5 = new Array();
            var tipo5 = new Array();
            while (cantidadderiegos >= 8) {
                fechainiJSON[p] = $("#datetimepicker_" + cantidadderiegos).val();
                sector5[p] = document.getElementById('sectores_' + cantidadderiegos).value;
                sector5[p] = id_sector[sector5[p]];
                tipoSec = document.getElementById('tipos_' + cantidadderiegos).value;
                if (tipoSec == 2) {
                    tiempofin = document.getElementById('duracionriego_' + cantidadderiegos).value;
                    fechafin = $("#datetimepicker_" + cantidadderiegos).val();
                    var fechafin = new Date(fechafin);
                    var res = tiempofin.split(":");
                    hora = res[0];
                    minutos = res[1];
                    minutos = parseInt(minutos);
                    hora = parseInt(hora);
                    hora = hora * 60 * 60;
                    minutos = minutos * 60;
                    segundos = hora + minutos;
                    fechafin.setSeconds(segundos);

                    dia = fechafin.getDate();
                    if (dia < 10) {
                        dia = "0" + dia;
                    }
                    mes = fechafin.getMonth() + 1;
                    if (mes < 10) {
                        mes = "0" + mes;
                    }
                    año = fechafin.getFullYear();
                    hora = fechafin.getHours();
                    if (hora < 10) {
                        hora = "0" + hora;
                    }
                    minuto = fechafin.getMinutes();
                    if (minuto < 10) {
                        minuto = "0" + minuto;
                    }
                    fechafinJSON[p] = año + "-" + mes + "-" + dia + " " + hora + ":" + minuto;
                    tipo5[p] = tipoSec;
                } else if (tipoSec == 1) {
                    i = cantidadderiegos + 100;
                    fechafinJSON[p] = $("#datetimepicker_" + i).val();
                    tipo5[p] = tipoSec;
                } else if (tipoSec == 3) {
                    fechafinJSON[p] = $("#volumeninput_" + cantidadderiegos).val();
                    tipo5[p] = tipoSec;
                }
                cantidadderiegos = cantidadderiegos - 1;
                p = p + 1;
            }

            tipo5 = JSON.stringify(tipo5);
            sector5 = JSON.stringify(sector5);
            fechainiJSON = JSON.stringify(fechainiJSON);
            fechafinJSON = JSON.stringify(fechafinJSON);
            bomba_continua = $('#bomba_continua').prop('checked');
            if(bomba_continua==true){
                bomba_continua = 1;
            }else{
                bomba_continua = 0;
            }
            iteracion = p - 1;
            var data = 'fechafinJSON=' + fechafinJSON + '&fechainiJSON=' + fechainiJSON + '&sector=' + sector5 + '&iteracion=' + iteracion + '&tipo5=' + tipo5 + '&bomba_continua=' + bomba_continua;;
            $.ajax({
                url: 'ajax/insertar_riego_json.php',
                type: 'post',
                data: data,
                beforeSend: function() {
                    console.log('enviando datos a la bd....')
                },
                success: function(resp) {
                    javascript: location.reload();
                }
            });
        }
        function excel() {
            var id_empresa = <?php echo $id_empresa; ?>;
            var equipo = <?php echo json_encode($equipo_post); ?>;
            var tipo = $('input:radio[name=equipo]:checked').val();

            fecha_iniExcel2 = document.getElementById('fechainiexcel').value;
            fecha_finExcel2 = document.getElementById('fechafinexcel').value;
            document.getElementById('id_equipoExcel').value = equipo;
            document.getElementById('empresaExcel').value = id_empresa;
            document.getElementById('fecha_iniExcel').value = fecha_iniExcel2;
            document.getElementById('fecha_finExcel').value = fecha_finExcel2;
            document.getElementById('equipoExcel2').value = tipo;

            document.getElementById('id_equipoExcel1').value = equipo;
            document.getElementById('empresaExcel1').value = id_empresa;
            document.getElementById('fecha_iniExcel1').value = fecha_iniExcel2;
            document.getElementById('fecha_finExcel1').value = fecha_finExcel2;
            document.getElementById('equipoExcel21').value = tipo;
            if (id_empresa != 95) {
                document.getElementById('myForm5').submit();
            } else {
                document.getElementById('myForm77').submit();
            }
        }
        function ping() {
            <!-- Ajax que muestra el estado de conexion con respecto al sistema local -->
            var data = 'id_empresa=' + id_empresa;
            var ping_net = 0;
            $.ajax({
                url: 'ajax/test_ping.php',
                type: 'post',
                data: data,
                async: false,
                beforeSend: function() {},
                success: function(resp) {
                    console.log(resp);
                    ping_net = parseInt(resp);
                    if (ping_net < 400) {
                        $("#net").attr("src", "imagenes/net_good.png");
                    } else if (ping_net < 800) {
                        $("#net").attr("src", "imagenes/net_medium.png");
                    } else {
                        $("#net").attr("src", "imagenes/net_bad.png");
                    }

                }
            });
            return ping_net;
            <!--///////////////////////////////////////////////////////////////////// -->
        }
        //*************************************************************************************************
        // funcion para zincronizar riegos desde servidor local antes de ejecutar alguna accion
        //**************************************************************************************************
        function sincronizar(nombre_funcion) {

            var id_empresa = <?php echo $id_empresa; ?>;
            var data = 'id_empresa=' + id_empresa;





            $.ajax({
                url: 'ajax/corroborar_riegos_local.php',
                type: 'post',
                data: data,
                beforeSend: function() {
                    //$('.bs-example-modal-sm').modal('show');
                },
                success: function(resp) {
                    //alert(resp.trim());
                    if (resp.trim() == 'si') {
                        $.ajax({
                            url: 'ajax/sincronizar_riegos_de_local.php',
                            type: 'post',
                            async: false,
                            data: data,
                            beforeSend: function() {

                                $('#traspaso_riegos').modal({
                                    backdrop: 'static'
                                })
                                $('#traspaso_riegos').modal('show');
                            },
                            success: function(resp) {
                                //holi=resp;
                                actualizarRiegos();
                                respuesta_lem_trasnmisor = manual_lemtransmisor();


                                setTimeout(function() {
                                    $("#traspaso_riegos").modal('hide');
                                }, 3000);

                                if (respuesta_lem_trasnmisor == false) {
                                    if (nombre_funcion == 'insertar') {

                                        $('#editarSensorTRB').modal('show');
                                    } else if (nombre_funcion == 'cancelar') {

                                        doDelete();
                                    } else if (nombre_funcion == 'reset') {

                                        activacionManualNodos();
                                        $('#editarNodos').modal('show');
                                    }
                                }


                            }
                        });
                        //alert('Si tengo riegos desde el local');
                    } else if (resp.trim() == '404') {
                        //alert('no tiene pc asociado');

                        //
                    } else {

                        //actualizarRiegos();
                        respuesta_lem_trasnmisor = manual_lemtransmisor();
                        if (respuesta_lem_trasnmisor == false) {
                            if (nombre_funcion == 'insertar') {

                                $('#editarSensorTRB').modal('show');
                            } else if (nombre_funcion == 'cancelar') {

                                pasaporte(3);
                            } else if (nombre_funcion == 'reset') {

                                activacionManualNodos();
                                $('#editarNodos').modal('show');
                            }
                        }
                        //alert('no tengo riegos del local');
                    }
                    //$('.bs-example-modal-sm').modal('hide');

                    //
                }
            });




        }
        function net_status(nombre_funcion) {
            var ping_net = ping();
            //alert(ping_net);
            if (ping_net < 900) {
                sincronizar(nombre_funcion);
            } else {
                $('#estado_internet').modal({
                    backdrop: 'static'
                })
                $('#estado_internet').modal('show');
            }
        }
        function manual_lemtransmisor() {
            <!-- Ajax que muestra el estado de lemboxtransmisor con respecto al sistema local -->
            var data = 'id_empresa=' + id_empresa + '&equipo=' + equipo_post;
            var ping_net = 0;
            var lemtransmisor = false;
            $.ajax({
                url: 'ajax/transmisor.php',
                type: 'post',
                data: data,
                async: false,
                beforeSend: function() {},
                success: function(resp) {

                    if (resp == 1) {

                        $('#modal_manual').modal({
                            backdrop: 'static'
                        })
                        $('#modal_manual').modal('show');

                        document.getElementById("new").style.display = "none";
                        lemtransmisor = true;
                    }

                }
            });
            return lemtransmisor;
        }
        function toggle_celular() {
            //Funcion que mueve el botton de agendar luego de clickear el menu en la version de celular timeinle

            if (click_celular == 0) {
                click_celular = 1;
                $("#new").css("margin-top", "164px");
            } else {
                click_celular = 0;
                $("#new").css("margin-top", "55px");
            }
        }
        function detectar_pausa() {
            //ajax Actualizar Riegos Programado
            $.ajax({
                url: 'ajax/pausarRiegos.php',
                type: 'post',
                data: 'id_empresa=' + id_empresa + '&id_equipo=' + equipo_post,
                beforeSend: function() {

                },
                success: function(resp) {
                    console.log(resp);
                    if (resp == 0) {
                        $('#play').hide();
                        $('#pausa').show();
                        swal("No existen Riegos para pausar", "success");
                    } else {
                        $('#pausa').hide();
                        $('#play').show();
                        javascript: location.reload();
                    }
                }
            });
        }
        function detectar_play() {
            //ajax Actualizar Riegos Programado
            $.ajax({
                url: 'ajax/playRiegos.php',
                type: 'post',
                data: 'id_empresa=' + id_empresa + '&id_equipo=' + equipo_post,
                beforeSend: function() {

                },
                success: function(resp) {
                    console.log(resp);
                    if (resp == 0) {
                        $('#play').hide();
                        $('#pausa').show();
                        swal("No existen Riegos para pausar", "success");
                    } else {
                        $('#pausa').hide();
                        $('#play').show();
                        javascript: location.reload();
                    }
                }
            });
        }
        function mostrar_modal_guardar_plan(){
            $('#plan_riegos').modal({backdrop: 'static'});
            $('#plan_riegos').modal('show');
        }
        function guardar_planificacion(){
            p = 0;
            var fechainiJSON = new Array();
            var fechafinJSON = new Array();
            var sector5 = new Array();
            var tipo5 = new Array();
            var grados = new Array();
            var cantidadderiegos_aux = cantidadderiegos;
            
            while (cantidadderiegos_aux >= 8) {

                fechainiJSON[p] = $("#datetimepicker_" + cantidadderiegos_aux).val();
                fechainiJSON[p] = moment(fechainiJSON[p]).format('YYYY-MM-DD HH:mm:00');

                console.log(cantidadderiegos_aux);
                css = document.getElementById('grado_' + cantidadderiegos_aux).style.visibility;
                if (css == "hidden") {
                    grados[p] = -1;
                } else {
                    grados[p] = $("#grado_" + cantidadderiegos_aux).val();
                }
                sector5[p] = document.getElementById('sectores_' + cantidadderiegos_aux).value;
                sector5[p] = id_sector[sector5[p]];
                tipoSec = document.getElementById('tipos_' + cantidadderiegos_aux).value;

                if (tipoSec == 2) {

                    tiempofin = document.getElementById('duracionriego_' + cantidadderiegos_aux).value;
                    var res = tiempofin.split(":");

                    hora = res[0];
                    minutos = res[1];
                    minutos = parseInt(minutos);
                    hora = parseInt(hora);

                    hora = hora * 60;
                    minutosSumados = hora + minutos;

                    fechafin = $("#datetimepicker_" + cantidadderiegos_aux).val();
                    fechafin = moment(fechafin);
                    fechafin = moment(fechafin).add(minutosSumados, 'minutes').format('YYYY-MM-DD HH:mm:00');

                    fechafinJSON[p] = fechafin;
                    tipo5[p] = tipoSec;

                } else if (tipoSec == 1) {

                    i = cantidadderiegos_aux + 100;

                    fechafinJSON[p] = $("#datetimepicker_" + i).val();
                    fechafinJSON[p] = moment(fechafinJSON[p]).format('YYYY-MM-DD HH:mm:00');

                    tipo5[p] = tipoSec;

                } else if (tipoSec == 3) {

                    fechafinJSON[p] = $("#volumeninput_" + cantidadderiegos_aux).val();
                    fechafinJSON[p] = moment(fechafinJSON[p]).format('YYYY-MM-DD HH:mm:00');

                    tipo5[p] = tipoSec;

                }

                cantidadderiegos_aux = cantidadderiegos_aux - 1;
                p = p + 1;

            }
            console.log(fechafinJSON); 
            var fecha_ini = moment(fechainiJSON[fechafinJSON.length - 1]);
            var fecha_fin = moment(fechafinJSON[0]);
            var semana = weekLabel(fecha_ini);
            var domingo = moment(semana[6]);
            console.log(domingo);
            fecha_fin = moment(fecha_fin).format('YYYY/MM/DD');
            domingo = moment(semana[6],'DD/MM/YYYY').format('YYYY/MM/DD');
            console.log(fecha_fin);
            console.log(domingo);
            if(fecha_fin <= domingo){
                grados = JSON.stringify(grados);
                tipo5 = JSON.stringify(tipo5);
                sector5 = JSON.stringify(sector5);
                fechainiJSON = JSON.stringify(fechainiJSON);
                fechafinJSON = JSON.stringify(fechafinJSON);
                iteracion = p - 1;
                bomba_continua = $('#bomba_continua').prop('checked');
                var nombre_planificacion = $('#nombre_planificacion_txt').val();
                var data = 'fechafinJSON=' + fechafinJSON + '&nombre_plan=' + nombre_planificacion + '&id_empresa=' + id_empresa + '&fechainiJSON=' + fechainiJSON + '&sector=' + sector5 + '&iteracion=' + iteracion + '&tipo5=' + tipo5 + '&grados=' + grados + '&bomba_continua=' + bomba_continua;
                $.ajax({
                    url: 'ajax/guardar_planificacion.php',
                    type: 'post',
                    data: data,
                    async: 'false',
                    beforeSend: function() {
                        $('.bs-example-modal-sm').modal('show');
                    },
                    success: function(resp) {
                        $('.bs-example-modal-sm').modal('hide');
                        sweetAlert({
                        type: 'success',
                        title: 'Tu nueva planificacion ha sido guardada',
                        showConfirmButton: false,
                        timer: 1500
                        })
                    }
                });
            }else{
                sweetAlert({
                        type: 'error',
                        title: 'Tu nueva planificacion no puede ser mayor a una semana',
                        showConfirmButton: false,
                        timer: 1800
                })    
            }       
        }
        function mostrar_modal_cargar_plan(){
            $('#cargar_plan_riegos').modal({backdrop: 'static'});
            $('#cargar_plan_riegos').modal('show');
            var data = 'id_empresa=' + id_empresa + '&id_equipo=' + equipo_post + '&id_caseta=' + caseta_post;
            $.ajax({
                url: 'ajax/cargar_planificaciones.php',
                type: 'post',
                data: data,
                async: 'false',
                beforeSend: function() {
                    $('.bs-example-modal-sm').modal('show');
                },
                success: function(resp) {
                    console.log(resp);
                    $('.bs-example-modal-sm').modal('hide');   
                    var planificaciones = JSON.parse(resp);
                    $("#select_planificaciones").empty();
                    $("#select_planificaciones").append('<option value="0" disabled selected>Seleccione un Programa:</option>');
                    i = planificaciones.length;
                    x = 0;
                    while (x < i) {
                        var o = new Option(planificaciones[x].nombre_planificacion, planificaciones[x].id_planificacion);
                        /// jquerify the DOM object 'o' so we can use the html method
                        $(o).html(planificaciones[x].nombre_planificacion);
                        $("#select_planificaciones").append(o);
                        x = x+1;
                    }                
                }
            });
        }
        function setFechaInicio(value){
            if(value == 1){
                var lunes = moment().startOf('isoWeek').add(1, 'week');
                lunes = moment(lunes).format('YYYY-MM-DD');  
                $('#fechaIniPlanificacion').val(lunes);
                $('#fechaIniPlanificacion').attr('readonly', true);
            }else{
                $('#fechaIniPlanificacion').attr('readonly', false); 
                var today = new Date().toISOString().split('T')[0];
                $("#fechaIniPlanificacion").attr('min', today);   
            }
        }
        function setFechaFin(value){
            //alert(value);
            //$("#fechaFinPlanificacion").attr('min', value);    
        }
        function previsualizar(){
            var error = validar_form_seleccionar_plan();
            if(error == 0){
                mostrar_visualizar_modal();
                $('.bs-example-modal-sm').modal('show');
                setTimeout(function(){
                    vista_programa_riego();
                    $('.bs-example-modal-sm').modal('hide');   
                }, 200);
                             
            }else{
                return
            }
            var ancho_timeline = $('.timeline-content').width();
            $('.timeline-content').width(ancho_timeline);
        }
        function validar_form_seleccionar_plan(){
            var errors = [];
            var html_msg = "";
            if($("#select_planificaciones").val() == null){
                errors.push('*Seleccione un Programa de Riego');
            }
            if($("#select_tipo_planificacion").val() == null){
                errors.push('*Seleccione un Tipo de Programa de Riego');
            }
            if($("#fechaIniPlanificacion").val() == ''){
                errors.push('*Seleccione una Fecha de Inicio del Programa de Riego');
            }
            if($("#repeticion_programacion").val() == 0){
                errors.push('*Seleccione una Fecha de Fin del Programa de Riego');
            }
            errors.forEach(function(element) {
                html_msg += element + "\n";
            });
            if(errors.length > 0){
                sweetAlert({
                        title: "Por favor complete las siguientes indicaciones",
                        type: "error",
                        text: html_msg,
                        showConfirmButton: true,
                })
                return 1;
            }else{
                return 0;
            }
        }
        function mostrar_visualizar_modal(){
            $('#vizualizar_plan_riego').modal({backdrop: 'static'});
            $('#vizualizar_plan_riego').modal('show');
        }
        function vista_programa_riego(){
            id_planificaciones_riegos = $("#select_planificaciones").val();
            tipo_plafinicacion = $("#select_tipo_planificacion").val();
            fecha_ini_planificacion = $("#fechaIniPlanificacion").val();
            repeticiones = $("#repeticion_programacion").val();
            var data = 'id_planificaciones_riegos=' + id_planificaciones_riegos + '&tipo_plafinicacion=' + tipo_plafinicacion + '&fecha_ini_planificacion=' + fecha_ini_planificacion + '&repeticiones=' + repeticiones;
                $.ajax({
                    url: 'ajax/programaRiegos.php',
                    type: 'post',
                    data: data,
                    async: true,
                    beforeSend: function() {},
                    success: function(resp) {
                        console.log(resp);
                        var riegos = JSON.parse(resp);
                        programa_plan_riegos = riegos;
                        var id_programacion_div = riegos[0].id_programacion_riego;
                        // Create and populate a data table.
                        data4 = new google.visualization.DataTable();
                        data4.addColumn('datetime', 'start');
                        data4.addColumn('datetime', 'end');
                        data4.addColumn('string', 'content');
                        data4.addColumn('string', 'group');
                        data4.addColumn('string', 'className');
                        // create some random data4
                        for (var n = 0, len = riegos.length; n < len; n++) {
                            id[n] = riegos[n].id_programacion_riego;
                            minT[n] = riegos[n].minT;
                            p_fecha_ini[n] = riegos[n].p_fecha_ini;
                            p_fecha_fin[n] = riegos[n].p_fecha_fin;
                            r_fecha_ini[n] = riegos[n].p_fecha_ini;
                            r_fecha_fin[n] = riegos[n].p_fecha_fin;
                            var id_programacion_div = riegos[n].id_programacion_riego;
                            var est = riegos[n].estado;
                            var up_p = riegos[n].update_p;
                            var name = riegos[n].sector;
                            var now = new Date();
                            var end = new Date();
                            if (est == 7 || est == 4 || est == 6 || est == 8) {

                                // Código original
                                //var end = new Date(riegos[n].r_fecha_fin);

                                // Código modificado
                                var end = moment(riegos[n].r_fecha_fin).toDate();

                            } else {

                                // Código original
                                //var end = new Date(riegos[n].p_fecha_fin);

                                // Código modificado
                                var end = moment(riegos[n].p_fecha_fin).toDate();

                            }
                            // Código original
                            //var start = new Date(riegos[n].p_fecha_ini);

                            // Código modificado
                            var start = moment(riegos[n].p_fecha_ini).toDate();


                            var duracion = end - start;
                            duracion = (((duracion / 1000) / 60) / 60);
                            var start2 = new Date(riegos[n].p_fecha_ini);
                            var end2 = new Date(riegos[n].r_fecha_fin);
                            var duracion2 = end2 - start2;
                            duracion2 = (((duracion2 / 1000) / 60) / 60);
                            ancho = duracion2 * 100;
                            ancho = ancho / duracion;
                            var start3 = new Date(riegos[n].r_fecha_ini);
                            var end3 = new Date(riegos[n].r_fecha_fin);
                            var duracion3 = end3 - start3;
                            duracion3 = (((duracion3 / 1000) / 60) / 60);
                            ancho2 = duracion3 * 100;
                            ancho2 = ancho2 / duracion;
                            var left = ancho - ancho2;
                            var ini = riegos[n].p_fecha_ini.split(" ");
                            var fin = riegos[n].p_fecha_fin.split(" ");
                            var imagen = "";
                            var cargando_p = " ";
                            if (est == 4) {
                                var color = 'rgb(113, 113, 102)';
                            }
                            if (est == 2) {
                                var color = 'green';
                            }
                            if (est == 1 || est == 33) {
                                var color = 'rgb(24, 85, 155)';
                            }
                            if (est == 12) {
                                var color = 'rgb(24, 85, 155)';
                                //var cargando_p = "<img src='imagenes/pausa_amarillo.png' style='width: 18px;position: relative;top: -17px;'>"
                            }
                            if (est == 3 || est == 7 || est == 8) {
                                var color = 'rgb(184, 23, 23)';
                                left = '0px';
                                ancho2 = '30px';
                            }
                            if (est == 6) {
                                var color = 'green; background: repeating-linear-gradient(45deg,  #606dbc,  #606dbc 10px,  #465298 10px,  #465298 20px)';
                            }
                            if (est == 5) {
                                var color = 'red';
                            }
                            if (est == 0) {
                                if (up_p == 0) {
                                    //var cargando_p = "<img src='imagenes/loading.gif' style='width: 18px;position: relative;top: -2px;'>"
                                }
                                var color = '#5D99C3';
                            }

                            // Codigo Original
                            //var availability ='<div style="border-width:4px;" onMouseOver=creainfo("' + n + '",event); onMouseOut=borrarinfo();>' + cargando_p + '<div onMouseOver=creainfo("' + n + '",event); onMouseOut=borrarinfo(); id="' + id_programacion_div + '" style=" width: ' + ancho2 + '%; margin-left:' + left + '%; background-color: ' + color + '; "id="i' + n + '";><div onMouseOver=creainfo("' + n + '",event); onMouseOut=borrarinfo(); style="color: #5D99C3;">&nbsp;</div></div>' + imagen + '</div>' + divsubriego + '';

                            // Codigo editado
                            if ((/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) || (/android/i.test(userAgent))) {
                                if (est == 12) {
                                    var availability = '<div style="border-width:4px;" ontouchstart=creainfo("' + n + '",event); ontouchend=borrarinfo();><div ontouchstart=creainfo("' + n + '",event); ontouchend=borrarinfo(); id="' + id_programacion_div + '" style=" width: ' + ancho2 + '%; margin-left:' + left + '%; background-color: ' + color + '; height:17px" id="i' + n + '";><div ontouchstart=creainfo("' + n + '",event); ontouchend=borrarinfo(); style="color: #5D99C3;">&nbsp;</div></div>' + imagen + '</div>' + divsubriego + '' + cargando_p + '';
                                } else {
                                    var availability = '<div style="border-width:4px;"ontouchstart=creainfo("' + n + '",event); ontouchend=borrarinfo();>' + cargando_p + '<div ontouchstart=creainfo("' + n + '",event); ontouchend=borrarinfo(); id="' + id_programacion_div + '" style=" width: ' + ancho2 + '%; margin-left:' + left + '%; background-color: ' + color + '; "id="i' + n + '";><div ontouchstart=creainfo("' + n + '",event); ontouchend=borrarinfo();  style="color: #5D99C3;">&nbsp;</div></div>' + imagen + '</div>' + divsubriego + '';
                                }                            
                            }else {
                                if (est == 12) {
                                    var availability = '<div style="border-width:4px;" onMouseOver=creainfo("' + n + '",event); onMouseOut=borrarinfo();><div onMouseOver=creainfo("' + n + '",event); onMouseOut=borrarinfo(); id="' + id_programacion_div + '" style=" width: ' + ancho2 + '%; margin-left:' + left + '%; background-color: ' + color + '; height:17px" id="i' + n + '";><div onMouseOver=creainfo("' + n + '",event); onMouseOut=borrarinfo(); style="color: #5D99C3;">&nbsp;</div></div>' + imagen + '</div>' + divsubriego + '' + cargando_p + '';
                                } else {
                                    var availability = '<div style="border-width:4px;" onMouseOver=creainfo("' + n + '",event); onMouseOut=borrarinfo();>' + cargando_p + '<div onMouseOver=creainfo("' + n + '",event); onMouseOut=borrarinfo(); id="' + id_programacion_div + '" style=" width: ' + ancho2 + '%; margin-left:' + left + '%; background-color: ' + color + '; "id="i' + n + '";><div onMouseOver=creainfo("' + n + '",event); onMouseOut=borrarinfo(); style="color: #5D99C3;">&nbsp;</div></div>' + imagen + '</div>' + divsubriego + '';
                                }
                            }

                            var group = availability.toLowerCase();
                            var content = availability;
                            data4.addRow([start, end, content, name, group]);
                            divsubriego = "";
                        }
                        // specify options
                        // specify options
                        var options = {
                            width: "100%",
                            eventMarginAxis: 0,
                            height: "100%",
                            layout: "box",
                            axisOnTop: true,
                            eventMargin: 10, // minimal margin between events
                            eventMarginAxis: 0, // minimal margin beteen events and the axis
                            editable: false,
                            showNavigation: true,
                            selectable: true,
                            locale: 'es',
                            showMajorLabels: true,
                            groupMinHeight: 20,
                            groupsOnRight: false,
                            zoomable: true,
                            stackEvents: false,
                            groupsChangeable: true,
                            zoomMin: 86400000,
                            zoomMax: 604800000
                        };
                        if (id_empresa == 95 || id_empresa == 129) {
                            var options = {
                                width: "100%",
                                eventMarginAxis: 0,
                                height: "100%",
                                layout: "box",
                                axisOnTop: true,
                                eventMargin: 10, // minimal margin between events
                                eventMarginAxis: 0, // minimal margin beteen events and the axis
                                editable: false,
                                showNavigation: true,
                                selectable: true,
                                locale: 'es',
                                showMajorLabels: true,
                                groupMinHeight: 20,
                                groupsOnRight: false,
                                zoomable: true,
                                stackEvents: false,
                                groupsChangeable: true,
                                zoomMin: 86400000,
                                zoomMax: 604800000,
                                groupsWidth: '150px'
                            };
                        }
                        // Instantiate our timeline object.
                        timeline3 = new links.Timeline(document.getElementById('mytimeline3'), options);
                        // register event listeners
                        // Draw our timeline with the created data4 and options
                        timeline3.draw(data4);
                        
                            restart_fecha2 = new Date(moment(riegos[0].r_fecha_fin).toDate() - 6 * 60 * 60 * 1000);
                        
                        
                            reend_fecha2 = new Date(moment(riegos[0].r_fecha_fin).toDate() + 12 * 60 * 60 * 1000);
                        
                        var start = new Date(restart_fecha2);
                        var end = new Date(reend_fecha2);
                        console.log(start);
                        console.log(end);
                        timeline3.setVisibleChartRange(start, end);
                    }
                })
        } 
        function eliminarPrograma(){
            clearInterval(repeticion2);
            swal({
                    title: "Escriba su clave de Administrador",
                    type: "input",
                    confirmButtonText: "Aceptar",
                    cancelButtonText: "Cancelar",
                    confirmButtonColor: "#37E52E",
                    showCancelButton: true,
                    closeOnConfirm: false,
                    animation: "slide-from-top"
                },
                function(inputValue) {
                    if (inputValue === false) {
                        repeticion2 = window.setInterval("actualizarRiegos()", 120000);
                        return false;
                    } else {
                        repeticion2 = window.setInterval("actualizarRiegos()", 120000);
                        if (inputValue === "") {
                            swal.showInputError("El campo no puede quedar vacio!");
                            return false
                        }
                        //swal("Nice!", "You wrote: " + inputValue, "success");
                        var clave_aux = document.getElementById('pass_aux').value;
                        $.ajax({
                            url: 'ajax/pass_control.php',
                            type: 'post',
                            data: 'clave=' + clave_aux + '&id_empresa=' + id_empresa,
                            beforeSend: function() {

                            },
                            success: function(resp) {
                                id_planificaciones_riegos = $("#select_planificaciones").val();
                                if(id_planificaciones_riegos == null){
                                    swal("", "Seleccione un Programa de Riego.", "error");
                                }else{
                                var data = 'id_planificaciones_riegos=' + id_planificaciones_riegos;
                                    $.ajax({
                                        url: 'ajax/eliminarProgramaRiegos.php',
                                        type: 'post',
                                        data: data,
                                        async: true,
                                        beforeSend: function() {},
                                        success: function(resp) {
                                            swal("", "Programa de Riego Eliminado.", "success");
                                            mostrar_modal_cargar_plan();
                                        }
                                    }); 
                                }
                            },
                            error: function(resp) {
                                alert("Unexpected error! Try again." + resp);
                            }
                        })
                    }
                }


            )               
        }
        function cargarPrograma(){
            var error = validar_form_seleccionar_plan();
            if(error == 0){
                if(cantidadderiegos > 8){
                    for (var i = 9; i <= cantidadderiegos; i++) {
                        $('#g_' + i).remove();
                    }
                }
                newid = 8;
                newid = parseInt(newid);
                $("#g_"+ newid).css("background-color", "rgb(210, 245, 210)");
                $("#datetimepicker_" + newid).val(moment(programa_plan_riegos[0].r_fecha_ini).format("YYYY-MM-DD HH:mm"));
                console.log(programa_plan_riegos);
                var fecha1 = moment(programa_plan_riegos[0].r_fecha_ini);
                var fecha2 = moment(programa_plan_riegos[0].r_fecha_fin);
                var minutos_programacion = fecha2.diff(fecha1, 'minutes') % 60;
                var horas_programacion = Math.trunc(fecha2.diff(fecha1, 'minutes') / 60);
                if(minutos_programacion < 10){
                    minutos_programacion = "0" + minutos_programacion;
                }
                if(horas_programacion < 10){
                    horas_programacion = "0" + horas_programacion;
                }
                var duracion_riego_programa = horas_programacion + ":" + minutos_programacion;
                document.getElementById('duracionriego_' + newid).value = duracion_riego_programa;
                for (let index = 1; index < programa_plan_riegos.length; index++) {
                    cantidadderiegos = cantidadderiegos + 1;
                    j = newid;
                    eid = newid;
                    eid = "e_" + eid;
                    aid = newid;
                    document.getElementById("" + eid + "").style.display = "none";
                    newid = parseInt(newid);
                    newid = newid + 1;
                    newid2 = newid + 100;
                    newpre2 = newid - 1;
                    newpre = newid2 - 1;

                    document.getElementById("a_" + aid).style.display = 'none';
                    document.getElementById('sectores_' + aid).disabled = "disabled";
                    document.getElementById('datetimepicker_' + aid).disabled = "disabled";
                    document.getElementById('tipos_' + aid).disabled = "disabled";
                    document.getElementById('duracionriego_' + aid).disabled = "disabled";
                    document.getElementById('datetimepicker_' + newpre).disabled = "disabled";
                    document.getElementById('volumeninput_' + aid).disabled = "disabled";
                    document.getElementById("aceptar").disabled = 'disabled';
                    document.getElementById("grado_" + aid).disabled = 'disabled';
                    if (tipoRiego[0] == 2) {
                        visible = "visible";
                    } else {
                        visible = "hidden";
                    }
                    $("#padremodal").append("<div class='col-md-12' id='g_" + newid + "'>" +
                        "<div class='col-lg-2'><select class='form-control' id='sectores_" + newid + "' onchange='validartiemporeal(id);'>" +
                        "</select></div>" +
                        "<div class='col-lg-3' id='fechaini_" + newid2 + " style='width: 191px;'>" +
                        "<div class='input-group date'>" +
                        "<input type='text' id='datetimepicker_" + newid + "' onChange='fechamin1(id); validartiemporeal(id);'  class='form-control' />" +
                        "</div>" +
                        "</div>" +
                        "<div class='col-md-2'>" +
                        "<select class='form-control' onChange='selecciontipo(id); validartiemporeal(id);' id='tipos_" + newid + "''>" +
                        "<option  value='2'>Duracion</option>" +
                        //"<option  value='1'>Fecha inicio</option>"+
                        "<option  value='3'>M3</option>" +
                        "</select>" +
                        "</div>" +
                        "<div class='col-md-3' id='fechafin_" + newid2 + "'' style='display:none'>" +
                        "<div class='input-group date' >" +
                        "<input type='text' id='datetimepicker_" + newid2 + "' class='form-control' onChange='validartiemporeal(id);' />" +
                        "</div>" +
                        "</div>" +
                        "<div class='col-md-2' style='display:block;'' id='duracion_" + newid2 + "''>" +
                        "<input id='duracionriego_" + newid + "'' value='00:00' type='time' class='form-control' onChange='validartiemporeal(id);'/>" +
                        "</div>" +
                        "<div class='col-md-1' style='visibility:" + visible + ";'' id='duracion_" + newid2 + "''>" +
                        "<input id='grado_" + newid + "'' value='0' type='input' class='form-control' onChange='validartiemporeal(id);'/>" +
                        "</div>" +
                        "<div class='col-md-2' style='display:none;'' id='volumen_" + newid2 + "''>" +
                        "<input id='volumeninput_" + newid + "'' placeholder='m3'type='text' class='form-control' onChange='validartiemporeal(id);'/>" +
                        "</div>" +
                        "<div class='col-md-1'><a id='a_" + newid + "' onclick='validartiemporeal(id); agregarnuevoriego(id); ' style='cursor:pointer;'><img class='hidden-xs' src='imagenes/agregar.png'" +
                        "style='width:28px;'></a>" +
                        "</div>" +
                        "<div class='col-md-1'><a id='e_" + newid + "'' onclick='eliminarnuevoriego(id);' style='cursor:pointer;'><img class='hidden-xs' src='imagenes/trash.png'" +
                        "style='width:28px;'></a>" +
                        "</div>");
                    var largo = nombre_sector_control.length;
                    for (i = 0; i < largo; i++) {
                        $("#sectores_" + newid).append(' <option value="' + i + '">' + nombre_sector_control[i] + '</option>');
                        if(programa_plan_riegos[index].sector == nombre_sector_control[i]){
                            var value_nombre_sector = i;
                        }
                    }
                    $("#sectores_" + newid).val(value_nombre_sector);
                    $("#datetimepicker_" + newid).kendoDateTimePicker({
                        value: new Date(Date.parse("1992-07-21 20:00")),
                        interval: 1,
                        format: 'yyyy-MM-dd HH:mm',
                        timeFormat: "HH:mm",
                        culture: "es-ES",
                        min: new Date()
                    });
                    $("#datetimepicker_" + newid2).kendoDateTimePicker({
                        value: new Date(),
                        interval: 1,
                        format: 'yyyy-MM-dd HH:mm',
                        timeFormat: "HH:mm",
                        culture: "es-ES",
                        min: new Date()
                    });

                    var id4 = newid2 - 1;
                    var newid4 = newid - 1;
                    tipoSec = document.getElementById('tipos_' + newid4).value;
                    tipoPrin = 2;
                    if (tipoPrin == 2) {
                        var fecha1 = moment(programa_plan_riegos[index].r_fecha_ini);
                        var fecha2 = moment(programa_plan_riegos[index].r_fecha_fin);

                        console.log(fecha2.diff(fecha1, 'minutes'), ' dias de diferencia');

                        var minutos_programacion = fecha2.diff(fecha1, 'minutes') % 60;
                        var horas_programacion = Math.trunc(fecha2.diff(fecha1, 'minutes') / 60);

                        if(minutos_programacion < 10){
                            minutos_programacion = "0" + minutos_programacion;
                        }
                        if(horas_programacion < 10){
                            horas_programacion = "0" + horas_programacion;
                        }

                        var duracion_riego_programa = horas_programacion + ":" + minutos_programacion;
                        console.log(duracion_riego_programa); 
                        tiempofinPrin = document.getElementById('duracionriego_' + newid4).value;
                        prinfechafin = $("#datetimepicker_" + newid4).val();

                        //  var prinfechafin = new Date(prinfechafin);
                        var prinfechafin = moment(prinfechafin).toDate();
                        var res = tiempofinPrin.split(":");
                        hora = res[0];
                        minutos = res[1];
                        minutos = parseInt(minutos);
                        hora = parseInt(hora);
                        hora = hora * 60 * 60;
                        minutos = minutos * 60;
                        segundos = hora + minutos + 60;
                        prinfechafin.setSeconds(segundos);


                        dia = prinfechafin.getDate();
                        if (dia < 10) {
                            dia = "0" + dia;
                        }
                        mes = prinfechafin.getMonth() + 1;
                        if (mes < 10) {
                            mes = "0" + mes;
                        }
                        año = prinfechafin.getFullYear();

                        hora = prinfechafin.getHours();
                        if (hora < 10) {
                            hora = "0" + hora;
                        }
                        minuto = prinfechafin.getMinutes();
                        if (minuto < 10) {
                            minuto = "0" + minuto;
                        }

                        prinfechafin = año + "-" + mes + "-" + dia + " " + hora + ":" + minuto;
                        // Código orinal
                        //$("#datetimepicker_" + newid).val(prinfechafin);
                        //$("#datetimepicker_" + newid2).val(prinfechafin);

                        // Código modificado
                        // $("#datetimepicker_" + newid).val( moment(prinfechafin).format("YYYY-MM-DD HH:mm").toDate() );
                        $("#datetimepicker_" + newid).val(moment(programa_plan_riegos[index].r_fecha_ini).format("YYYY-MM-DD HH:mm"));

                        // $("#datetimepicker_" + newid2).val( moment(prinfechafin).format("YYYY-MM-DD HH:mm").toDate() );
                        $("#datetimepicker_" + newid2).val(moment(prinfechafin).format("YYYY-MM-DD HH:mm"));
                        
                        document.getElementById('duracionriego_' + newid).value = duracion_riego_programa;

                    }else if (tipoPrin == 1) {
                        $("#datetimepicker_" + newid).val(moment($("#datetimepicker_" + id4).format("YYYY-MM-DD HH:mm")).val());
                        $("#datetimepicker_" + newid2).val(moment($("#datetimepicker_" + id4).format("YYYY-MM-DD HH:mm")).val());
                        //prinfechafin = $("#datetimepicker_"+id2).val();
                    }
                    
                    //newid= newid + 1;       
                    $("#g_"+ newid).css("background-color", "rgb(210, 245, 210)");              
                }
                //$("#g_"+ newid).remove();
                $('#cargar_plan_riegos').modal('hide');
                $('#vizualizar_plan_riego').modal('hide');
                $("#aceptar").prop('disabled', false);             
            }
        }
        function limpiar_riegos(){ 
            console.log(cantidadderiegos);           
            for (var i = 9; i <= cantidadderiegos; i++) {
                $('#g_' + i).remove();
            }
            cantidadderiegos = 8;
            $( "#sectores_8" ).prop( "disabled", false );
            $( "#fechafin_8" ).prop( "disabled", false );
            $( "#datetimepicker_8" ).prop( "disabled", false );
            $( "#tipos_8" ).prop( "disabled", false );
            $( "#grado_8" ).prop( "disabled", false );
            $( "#duracionriego_8" ).prop( "disabled", false );
            $( "#volumeninput_8" ).prop( "disabled", false );    
        }
        function weekLabel(current) {
            const week = [];
            const weekFormat = [];
            current = moment(current).toDate();
            if(current.getDay() == 0){//En los casos en que es domingo, restar como si fuera septimo dia y no cero
                current.setDate(((current.getDate() - 7) + 1));
            }else{
                current.setDate(((current.getDate() - current.getDay()) + 1));
            }

            for (let i = 0; i < 7; i++) {
                week.push(new Date(current));
                current.setDate(current.getDate()+1);
            }
            week.forEach((w) => {
                weekFormat.push(moment(w).format('DD/MM/YYYY'));
            });
            return weekFormat;
        }
        //------------------------datepickers para agendar y escojer el dia-------------------------//          
        $('#datetimepicker_8').kendoDateTimePicker({
            value: new Date(),
            interval: 1,
            format: 'yyyy-MM-dd HH:mm',
            timeFormat: "HH:mm",
            culture: "es-ES",
            min: new Date()
        });
        var datepicker2 = $("#datetimepicker_8").data("kendoDateTimePicker");

        $("#datetimepicker_108").kendoDateTimePicker({
            value: new Date(),
            interval: 1,
            format: 'yyyy-MM-dd HH:mm',
            timeFormat: "HH:mm",
            culture: "es-ES"
        });
        var datepicker3 = $("#datetimepicker_108").data("kendoDateTimePicker");
        datepicker3.enable(true);

        $('#datetimepicker4').kendoDateTimePicker({
            value: new Date(),
            interval: 1,
            format: 'yyyy-MM-dd HH:mm',
            timeFormat: "HH:mm",
            culture: "es-ES",
            min: new Date()
        });
        var datepicker4 = $("#datetimepicker4").data("kendoDateTimePicker");

        $('#datetimepicker5').kendoDateTimePicker({
            value: new Date(),
            interval: 1,
            format: 'yyyy-MM-dd HH:mm',
            timeFormat: "HH:mm",
            culture: "es-ES"
        });
        var datepicker5 = $("#datetimepicker5").data("kendoDateTimePicker");
        datepicker5.enable(false);

        $('#datetimepicker6').kendoDatePicker({
            value: new Date(),
            culture: "es-ES",
            format: 'yyyy-MM-dd'
        });
        var datepicker6 = $("#datetimepicker6").data("kendoDateTimePicker");
        $('#datetimepicker7').kendoDateTimePicker({
            value: new Date(),
            interval: 1,
            format: 'yyyy-MM-dd HH:mm',
            timeFormat: "HH:mm",
            culture: "es-ES"
        });
        $('input:checkbox').change(function() {
        });
        $(document).ready(function() {
            detectar_pausa_play();
            manual_lemtransmisor();
            var newDate = moment();
            restart_fecha = moment(newDate).subtract(740, 'minutes');
            reend_fecha = moment(newDate).add(740, 'minutes');

            if (descripcionCaudal == null) {
                $("#divContenedor").css("max-height", "none");
                $("#divContenedor").css("height", "97%");

                $("#mytimeline2").css("max-height", "none");
                $("#mytimeline2").css("height", "97%");
            }
            if (celular == 1) {
                $("#divContenedor").css("max-height", "none");
                $("#divContenedor").css("height", "97%");
                $("#divContenedor").css("overflow", "hidden");

                $("#new").css("margin-top", "55px");
                $("#new").css("left", "23px");
            }
            $("#save").click(function(e) {
                e.preventDefault();
            });
            var id_sectorTotal = <?php echo json_encode($id_sectorTotal); ?>;
            if (id_sectorTotal.length < 11) {
                document.getElementById("divContenedor").style.overflow = "hidden";
            }
            var swValvulas = <?php echo json_encode($swValvulas); ?>;
            var tipoderiego = 3;
            var hoy = <?php echo $hoy; ?>;
            date = new Date();
            milliseconds = date.getTime();
            seconds = milliseconds / 1000 / 60;
            seconds = seconds.toFixed();
            hoy = hoy.toFixed();
            difhoralocalhoraserver = seconds - hoy;
            difhoralocalhoraserver = Math.abs(difhoralocalhoraserver);

            if (swValvulas == 0) {
                document.getElementById("actualizar").style.display = "none";
            }

            if (difhoralocalhoraserver > 1) {
                swal("Por Favor verifique la hora de su computador", "Hora oficial Chile: http://www.horaoficial.cl/")
            }
            var repeticion;
            var swReset = 0
            //------------------------datepickers para escojer por fecha-------------------------//
            $('#fecha_inicio').kendoDatePicker({
                value: new Date(),
                format: 'yyyy-MM-dd',
                culture: "es-ES",
                max: new Date()

            });
            var fecha_inicio = $("#fecha_inicio").data("kendoDatePicker");
            $('#fecha_fin').kendoDatePicker({
                value: new Date(),
                format: 'yyyy-MM-dd',
                culture: "es-ES",
                max: new Date()
            });


        });
    </script>
</body>

</html>