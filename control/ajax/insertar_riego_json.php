<?php
include("conexion.php");
$tipo5 = $_POST['tipo5'];

$sector = $_POST['sector'];
$fechainiJSON = $_POST['fechainiJSON'];
$fechafinJSON = $_POST['fechafinJSON'];
$iteracion = $_POST['iteracion'];
$bomba_continua = $_POST['bomba_continua'];
$tipo_sector = $_POST['tipo_sector'];
$config_fertil = $_POST['config_fertil'];
$diccionario_tipo = $_POST['diccionario_tipo'];
$largo_lista = $iteracion;
$tipo5 = json_decode($tipo5);
$sector = json_decode($sector);
$fechainiJSON = json_decode($fechainiJSON);
$fechafinJSON = json_decode($fechafinJSON);
$tipo_sector = json_decode($tipo_sector);
$diccionario_tipo = json_decode($diccionario_tipo);
$config_fertil = json_decode($config_fertil, true);
$id_fertilizantes = array();
$id_sectores      = array();
while($iteracion >= 0){
    //if($tipo5[$iteracion] !=3){
    if($diccionario_tipo[$tipo_sector[$iteracion]] != 'fertilriego'){
        guardar_sector_tiempo($iteracion,$tipo5[$iteracion]); 
    }else{
        guardar_fertil_tiempo($iteracion,$tipo5[$iteracion]);
    }
    $iteracion = $iteracion -1;
}
detectar_fertilizante(0);
function guardar_sector_tiempo($iteracion,$tipo_riego){
    global $tipo5;
    global $sector;
    global $fechainiJSON;
    global $fechafinJSON;
    global $link;
    global $bomba_continua;
    global $id_sectores;
    $sql = "select caseta,equipo,nombre_sector,id_nodo,id_empresa from control_riego where id_sector = '$sector[$iteracion]' order by orden";
    $consulta = mysql_query($sql, $link) or die(mysql_error());
    if ($datatmp = mysql_fetch_array($consulta)) {
        $nombre_sector = $datatmp['nombre_sector'];
        $id_nodo = $datatmp['id_nodo'];
        $id_empresa = $datatmp['id_empresa'];
        $equipo = $datatmp['equipo'];
        $caseta = $datatmp['caseta'];
    }

    $sql = "select red from nodo where id_nodo = $id_nodo";
    $consulta = mysql_query($sql, $link) or die(mysql_error());
    if ($datatmp = mysql_fetch_array($consulta)) {
        $red = $datatmp['red'];
    }
    $sql = "select max(n_identificador) from programacion_riego where id_empresa = '$id_empresa' and equipo = $equipo";
    $consulta = mysql_query($sql, $link) or die(mysql_error());
    if ($datatmp = mysql_fetch_array($consulta)) {
        $n_identificadormax = $datatmp['max(n_identificador)'];
        $n_identificador = $n_identificadormax + 1;
        }
        

    $sql = "select max(id_programacion_riego) from programacion_riego where id_empresa = '$id_empresa'";
    $consulta = mysql_query($sql, $link) or die(mysql_error());
    if ($datatmp = mysql_fetch_array($consulta)) {
        $lipe = $datatmp['max(id_programacion_riego)']; 
            $id_programacion_riego = $lipe +1;        
    }else{
        $id_programacion_riego = 1;
    }

    $sql2 = "UPDATE control_riego SET  estado = 0,iteracion = 0 WHERE id_sector = '$sector[$iteracion]'";
    $consulta = mysql_query($sql2, $link);
    if($bomba_continua == 1){
        $fechafinJSON[$iteracion] = date("Y-m-d H:i:s", (strtotime($fechafinJSON[$iteracion]) + 300));     
    }   
    $sql = "INSERT INTO programacion_riego (red,id_programacion_riego,caseta,equipo,id_empresa,n_identificador,n_orden_riego_sector,id_nodo,p_fecha_ini,p_fecha_fin,r_fecha_ini,r_fecha_fin,sector,estado,id_sector) 
    VALUES($red,$id_programacion_riego,$caseta,$equipo,$id_empresa,$n_identificador,1,$id_nodo,'$fechainiJSON[$iteracion]','$fechafinJSON[$iteracion]',
    '$fechainiJSON[$iteracion]','$fechainiJSON[$iteracion]','$nombre_sector',0,$sector[$iteracion])";
    $consulta = mysql_query($sql, $link) or die(mysql_error());
    $id_sectores[$iteracion] = $id_programacion_riego;
}
function guardar_fertil_tiempo($iteracion,$tipo_riego){
    global $tipo5;
    global $sector;
    global $fechainiJSON;
    global $fechafinJSON;
    global $link;
    global $config_fertil;
    global $id_fertilizantes;

    if($tipo_riego == 3){
        $volumen = $fechafinJSON[$iteracion];
        $sql = "select caudal_pulso,caudal_inferior,pulso_minuto from control_riego where id_sector = '$sector[$iteracion]' and caudal_pulso != 0 group by id_sector";
        $consulta = mysql_query($sql,$link) or die(mysql_error());
        if ($datatmp = mysql_fetch_array($consulta)){
            $caudal_pulso = $datatmp['caudal_pulso'];//cn
            $caudal_inferior = $datatmp['caudal_inferior'];//ci
            $pn = $volumen/$caudal_pulso;
            $pi = $caudal_inferior/$caudal_pulso;
            $tmax = $pn/$pi;
            $fecha= strtotime($fechainiJSON[$iteracion]);
            $minutos = round($tmax);
            $minutos = $minutos*60;
            $fecha+=$minutos;
            $fechafinJSON[$iteracion] = date("Y-m-d H:i:s", $fecha ); 
        }
    }else{
        $pn = 0;
    }

    $sql = "select nombre_sector,id_empresa,equipo,caseta,id_nodo from control_riego where id_sector = '$sector[$iteracion]'";
    $consulta = mysql_query($sql, $link) or die(mysql_error());
    if ($datatmp = mysql_fetch_array($consulta)) {
        $nombre_fertilriego = $datatmp['nombre_sector'];
        $id_nodo = $datatmp['id_nodo'];
        $id_empresa = $datatmp['id_empresa'];
        $equipo = $datatmp['equipo'];
        $caseta = $datatmp['caseta'];
    } 

    $sql = "select red from nodo where id_nodo = $id_nodo";
    $consulta = mysql_query($sql, $link) or die(mysql_error());
    if ($datatmp = mysql_fetch_array($consulta)) {
        $red = $datatmp['red'];
    }
    $sql = "select max(n_identificador) from programacion_riego where id_empresa = '$id_empresa' and equipo = $equipo";
    $consulta = mysql_query($sql, $link) or die(mysql_error());
    if ($datatmp = mysql_fetch_array($consulta)) {
        $n_identificadormax = $datatmp['max(n_identificador)'];
        $n_identificador = $n_identificadormax + 1;
    }        

    $sql = "select max(id_programacion_riego) from programacion_riego where id_empresa = '$id_empresa'";
    $consulta = mysql_query($sql, $link) or die(mysql_error());
    if ($datatmp = mysql_fetch_array($consulta)) {
        $lipe = $datatmp['max(id_programacion_riego)']; 
            $id_programacion_riego = $lipe +1;        
    }else{
        $id_programacion_riego = 1;
    }

    $agitadora = 1;
    $bm = $config_fertil[$iteracion]['bm'];
    $bf = $config_fertil[$iteracion]['bf'];
    $delay_ini = $config_fertil[$iteracion]['delay_ini'];
    $delay_fin = $config_fertil[$iteracion]['delay_fin'];
    $p0 = $config_fertil[$iteracion]['p0'];

    $sql = "INSERT INTO `configuracion_fertil_riegos`(`id_sector_fertil_riego`, `agitador`, `bm`,`bf`,`delay_ini`, `delay_fin`, `id_fertil_riego`) VALUES ('$id_programacion_riego ',$agitadora,$bm,$bf,$delay_ini,$delay_fin,$p0)";
    $consulta = mysql_query($sql, $link) or die(mysql_error());

    $sql = "INSERT INTO programacion_riego (pulsos,red,id_programacion_riego,caseta,equipo,id_empresa,n_identificador,n_orden_riego_sector,id_nodo,p_fecha_ini,p_fecha_fin,r_fecha_ini,r_fecha_fin,sector,estado,id_sector) 
    VALUES($pn,$red,$id_programacion_riego,$caseta,$equipo,$id_empresa,$n_identificador,1,$id_nodo,'$fechainiJSON[$iteracion]','$fechafinJSON[$iteracion]',
    '$fechainiJSON[$iteracion]','$fechainiJSON[$iteracion]','$nombre_fertilriego',0,$sector[$iteracion])";
    $consulta = mysql_query($sql, $link) or die(mysql_error());

    $id_fertilizantes[$iteracion] = $id_programacion_riego;
}
function detectar_fertilizante($iteracion){
    global $diccionario_tipo;
    global $config_fertil;
    global $tipo_sector;
    global $largo_lista;
    global $id_fertilizantes;
    global $id_sectores;
    global $link;
    global $fechainiJSON;
    global $fechafinJSON;

    var_dump($id_sectores);
    var_dump($id_fertilizantes); 
    while ($iteracion <= $largo_lista) {
        if($diccionario_tipo[$tipo_sector[$iteracion]] != 'fertilriego'){
            $indice = $iteracion - 1;             
            while ($indice >= 0) {
                
                if($diccionario_tipo[$tipo_sector[$indice]] != 'fertilriego'){
                    $indice = -1; 
                }else{
                    $sql2 = "UPDATE configuracion_fertil_riegos SET id_programacion_riego = $id_sectores[$iteracion] WHERE id_sector_fertil_riego = $id_fertilizantes[$indice]";
                    $consulta = mysql_query($sql2, $link);
                }
                $indice--;
            }
        }
        $iteracion++; 
    }
}